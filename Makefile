#-----------------------------------------------------------------------------------------
# include the user config for system env. things like gcc 
include User.config

#-----------------------------------------------------------------------------------------
# functions
#-----------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------------
# currentDir
# returns the directory of the last included file
#-----------------------------------------------------------------------------------------
define currentDir
$(dir $(lastword $(MAKEFILE_LIST)))
endef


#-----------------------------------------------------------------------------------------
# updateLibFiles
# evaluates the LIB_SOURCES and LIB_INCLUDES variables, since some of them are based on 
# the DIR variable
#-----------------------------------------------------------------------------------------
define updateLibFiles
$(eval LIB_SOURCES  := $(LIB_SOURCES));
$(eval LIB_INCLUDES := $(LIB_INCLUDES));
endef

#-----------------------------------------------------------------------------------------
# updateLibFiles
# evaluates the USER_SOURCES and USER_INCLUDES variables, since some of them are based on 
# the DIR variable
#-----------------------------------------------------------------------------------------
define updateUserFiles
$(eval USER_SOURCES  := $(USER_SOURCES));
$(eval USER_INCLUDES := $(USER_INCLUDES));
endef

#-----------------------------------------------------------------------------------------
# updateFlags
# evaulates the $ variables in the LDFLAGS and CFLAGS variable, since some of them are
# based on the DIR variable
#-----------------------------------------------------------------------------------------
define updateFlags
$(eval LDFLAGS := $(LDFLAGS));
$(eval CFLAGS  := $(CFLAGS));
endef


#-----------------------------------------------------------------------------------------
# includeCommon
# includes the COMMON.config from the same directory and sets the dir variable
#-----------------------------------------------------------------------------------------
define includeCommon
$(eval DIR := $(call currentDir));
$(eval include $(DIR)COMMON.config); 
endef
#-----------------------------------------------------------------------------------------



#-----------------------------------------------------------------------------------------
# basic init stuff 
#-----------------------------------------------------------------------------------------
MODULES =
DEFINES = 
USER_LIBS = 
LDFLAGS =
CFLAGS = 
LIB_INCLUDES  = 
LIB_SOURCES   =
LIBS = 
LOGS = 
BUILD_DIR = build/
USER_INCLUDES =
USER_SOURCES =
SIZE_FORMAT = berkeley
SIZE_RADIX = 10
PROJECT = $(shell cat project)

include projects/$(PROJECT)/Makefile.config

ifndef TARGET
  TARGET = $(PROJECT)
endif
#----------------------------------------------------------------------------------------- 
 
 
#-----------------------------------------------------------------------------------------
# environment init 
#-----------------------------------------------------------------------------------------
export C_INCLUDE_PATH:=$(GCC_PATH)arm-none-eabi/include:$(GCC_PATH)lib/gcc/arm-none-eabi/$(GCC_VERSION)/include:$(GCC_PATH)lib/gcc/arm-none-eabi/$(GCC_VERSION)/include-fixed

TRGT = $(GCC_PATH)bin/arm-none-eabi-
CC   = $(TRGT)gcc
AS   = $(TRGT)gcc -x assembler-with-cpp
COPY = $(TRGT)objcopy
DUMP = $(TRGT)objdump
SIZE = $(TRGT)size

ifeq ($(BUILD),DEBUG)
	DEBUG = 1
	DEFINES += DEBUG
else
  ifeq ($(BUILD),RELEASE)
	RELEASE = 1
	DEFINES += RELEASE
  else
  	$(error No buildmode set, set BUILD = RELEASE or DEBUG)
  endif
endif
#-----------------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------------
# device and board config check and include 
#-----------------------------------------------------------------------------------------
ifndef BOARD
  $(error No board selected)
endif

ifndef CARRIER
  $(error No carrier selected)
endif

ifndef DEVICE
  $(error No device selected)
endif

DEVICE_CONFIG  = configs/devices/$(DEVICE)/$(BUILD).config
CARRIER_CONFIG = configs/carrier/$(CARRIER)/$(BUILD).config
BOARD_CONFIG   = configs/boards/$(BOARD)/$(BUILD).config
PROJECT_PATH   = projects/$(PROJECT)/

ifeq ($(wildcard $(BOARD_CONFIG)),)
  $(error No config for board $(BOARD) in mode $(BUILD) found)
endif

ifeq ($(wildcard $(CARRIER_CONFIG)),)
  $(error No config for carrier $(CARRIER) in mode $(BUILD) found)
endif

ifeq ($(wildcard $(DEVICE_CONFIG)),)
  $(error No config for device $(DEVICE) in mode $(BUILD) found)
endif

DEFINES += BOARD_$(BOARD)
DEFINES += $(DEVICE)
DEFINES += $(CARRIER)

include $(DEVICE_CONFIG)
include $(CARRIER_CONFIG)
include $(BOARD_CONFIG)
#-----------------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------------
# filter out double lib includes, and import the makefiles of the selected libraries
# this code is a piece of shit, but makefiles don't offer many alternatives
#-----------------------------------------------------------------------------------------
LIBS_FILTERED =
addLibToIncludes = $(eval LIBS_FILTERED+=$(LIB))
includeLibrary = $(eval include libs/$(LIB)/$(BUILD).config);
$(foreach LIB,$(USER_LIBS),$(if $(filter $(LIB),$(LIBS_FILTERED)),,$(call addLibToIncludes)))
$(foreach LIB,$(LIBS_FILTERED),$(if $(wildcard libs/$(LIB)/$(BUILD).config),$(call includeLibrary),$(error No makefile for library $(LIB) in mode $(BUILD) not found)))
#-----------------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------------
# convert log levels to defines for the debug header file
#-----------------------------------------------------------------------------------------
DEFINES += $(patsubst %,DEBUG_LOG_%,$(LOGS))
#

#-----------------------------------------------------------------------------------------
# add project files
#-----------------------------------------------------------------------------------------
PROJECT_INCLDUES = $(PROJECT_PATH)
PROJECT_SOURCES  = $(wildcard $(PROJECT_PATH)*.c)
#-----------------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------------
# add module files
#-----------------------------------------------------------------------------------------
MODULE_INCLUDES = modules/
MODULE_SOURCES  = 
addModuleFiles = $(eval MODULE_SOURCES+=$(wildcard modules/$(MODULE)*.c))
$(foreach MODULE,$(MODULES),$(if $(wildcard modules/$(MODULE)*.c),$(call addModuleFiles),$(warning No files found for module '$(MODULE)')))
#-----------------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------------
# build final include, source, obj list, defines 
#-----------------------------------------------------------------------------------------
INCLUDES = 
INCLUDES += $(MODULE_INCLUDES)
INCLUDES += $(USER_INCLUDES)
INCLUDES += $(PROJECT_INCLUDES)
INCLUDES += $(LIB_INCLUDES)

SOURCES =
SOURCES += $(MODULE_SOURCES)
SOURCES += $(USER_SOURCES)
SOURCES += $(PROJECT_SOURCES)
SOURCES += $(LIB_SOURCES)

OBJECTS = $(patsubst %.c,$(BUILD_DIR)%.o,$(SOURCES))

INCLUDES_ = $(patsubst %,-I%,$(INCLUDES))
DEFINES_  = $(patsubst %,-D%,$(DEFINES))

HEX_FILE = $(BUILD_DIR)$(TARGET).hex
ELF_FILE = $(BUILD_DIR)$(TARGET).elf
MAP_FILE = $(BUILD_DIR)$(TARGET).map
SIZE_INF = $(BUILD_DIR)$(TARGET).size
TIME_INF = $(BUILD_DIR)$(TARGET).time

TARGET_FILES = 
TARGET_FILES += $(HEX_FILE)
TARGET_FILES += $(ELF_FILE)
TARGET_FILES += $(MAP_FILE)
TARGET_FILES += $(SIZE_INF)
TARGET_FILES += $(TIME_INF)

-include $(OBJECTS:%.o=%.d)
#-----------------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------------
# target: version
#-----------------------------------------------------------------------------------------
.PHONY: version
version:
	@echo version of used gcc:
	@echo 
	@$(CC) --version
#-----------------------------------------------------------------------------------------
	
	
#-----------------------------------------------------------------------------------------
# target: clean
#-----------------------------------------------------------------------------------------
.PHONY: clean
clean:
	@rm -rf $(BUILD_DIR)
#-----------------------------------------------------------------------------------------
	
	
#-----------------------------------------------------------------------------------------
# target: source files
#-----------------------------------------------------------------------------------------
$(BUILD_DIR)%.o: %.c
	@echo compile: $<
	@mkdir -p $(dir $@)
	@$(CC) $(INCLUDES_) $(DEFINES_) $(CFLAGS) -c -Wa,-adhlns="$(@:.o=.lst)" -MD -MP -o $@ $<
#-----------------------------------------------------------------------------------------
	
	
#-----------------------------------------------------------------------------------------
# target: size
#-----------------------------------------------------------------------------------------
$(SIZE_INF): $(ELF_FILE)
	@echo generate size information
	@$(SIZE) --format=$(SIZE_FORMAT) --radix=$(SIZE_RADIX) $< | tee $(SIZE_INF)

.PHONY: size
size: $(SIZE_INF)
#-----------------------------------------------------------------------------------------
	
	
#-----------------------------------------------------------------------------------------
# target: linking (elf file)
#-----------------------------------------------------------------------------------------
$(ELF_FILE): $(OBJECTS)
	@echo
	@echo compiled: $(PROJECT)
	@echo link: $(notdir $@)
	@$(CC) $(LDFLAGS) -Wl,-Map,$(MAP_FILE) -o $@ $(OBJECTS) $(LIBS)
#-----------------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------------
# target: hex file
#-----------------------------------------------------------------------------------------
$(HEX_FILE): $(ELF_FILE)
	@echo
	@echo generate hexfile
	@$(COPY) -O ihex $< $@
#-----------------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------------
# target: all
#-----------------------------------------------------------------------------------------
.PHONY: all
all: eclipse_settings $(ELF_FILE) $(HEX_FILE) $(SIZE_INF)	
#-----------------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------------
# target: eclipse_settings
#-----------------------------------------------------------------------------------------
.PHONY: eclipse_settings
eclipse_settings: 
	@echo _eclipse_settings_ $(INCLUDES_) $(DEFINES_) a.c
#-----------------------------------------------------------------------------------------
	
	
#-----------------------------------------------------------------------------------------
# target: program_openocd
# downloads the program to the target uc
# it uses the openOCD for communicating with the target,
# command: openocd --file openocd.cfg
#-----------------------------------------------------------------------------------------
.PHONY: program_openocd
program_openocd: $(HEX_FILE) $(SIZE_INF)
	@echo start flashing the uc
	$(TRGT)gdb -ex "target remote localhost:3333" \
		-ex "monitor reset halt" \
		-ex "monitor stm32f1x mass_erase 0" \
		-ex "monitor flash write_image $(HEX_FILE) 0x00 ihex" \
		-ex "monitor sleep 500" \
		-ex "monitor reset run" \
		-ex "quit"
#-----------------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------------
# target: program_jlink
# downloads the program to the target uc
# it uses the jlink gdb server, just start it with the command: JLinkGDBServer
#-----------------------------------------------------------------------------------------
.PHONY: program_jlink
program_jlink: $(ELF_FILE) $(SIZE_INF)
	@echo start flashing the uc
	$(TRGT)gdb -ex "target remote localhost:2331" \
		-ex "monitor endian little" \
		-ex "monitor flash device = $(DEVICE)" \
		-ex "monitor speed auto" \
		-ex "monitor reset" \
		-ex "load $(ELF_FILE)" \
		-ex "monitor reset 2" \
		-ex "quit"
#-----------------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------------
# target: reset
#-----------------------------------------------------------------------------------------
.PHONY: reset
reset:
	@echo reset
	$(TRGT)gdb -ex "target remote localhost:2331" \
		-ex "monitor speed auto" \
		-ex "monitor reset 2" \
		-ex "monitor sleep 100" \
		-ex "monitor go" \
		-ex "quit"
#-----------------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------------
# target: start
#-----------------------------------------------------------------------------------------
.PHONY: start
start:
	@echo start device
	$(TRGT)gdb -ex "target remote localhost:2331" \
		-ex "monitor speed auto" \
		-ex "monitor go" \
		-ex "quit"
#-----------------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------------
# target: updateDef
#-----------------------------------------------------------------------------------------
.PHONY: updateDef
updateDef:
	@echo Updating module_structs files
	@cd ./modulebuilder/ && make
	@./modulebuilder/modulebuilder $(MODULES_DEF_DIR)modules_def.h > $(MODULES_DEF_DIR)modules_structs.h h
	@./modulebuilder/modulebuilder $(MODULES_DEF_DIR)modules_def.h > $(MODULES_DEF_DIR)modules_structs.c c
#-----------------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------------
# target: includePaths
# creates a xml file which can be imported in eclipse
#-----------------------------------------------------------------------------------------
define INCLUDE_BODY
<?xml version="1.0" encoding="UTF-8"?>
<cdtprojectproperties>
<section name="org.eclipse.cdt.internal.ui.wizards.settingswizards.IncludePaths">
<language name="Assembly Source File">
</language>
<language name="C Source File">
<includepath>$(GCC_PATH)arm-none-eabi/include</includepath>
<includepath>$(GCC_PATH)lib/gcc/arm-none-eabi/$(GCC_VERSION)/include</includepath>
<includepath>$(GCC_PATH)lib/gcc/arm-none-eabi/$(GCC_VERSION)/include-fixed</includepath>
</language>
</section>
</cdtprojectproperties>
endef
export INCLUDE_BODY
#-----------------------------------------------------------------------------------------
.PHONY: includePaths
includePaths:
	@echo "Generating include.xml inside your workspace"
	@echo "you can import it in C/C++ General -> Paths / Symbols"
	@echo "$$INCLUDE_BODY" > includes.xml
#-----------------------------------------------------------------------------------------

