#include "its5215l.h"
#include "gpio.h"
#include "debug.h"
#include "delay.h"

void ITS5215L_init( const ITS5215L_MyDef* its )
{
	GPIO_MyInitDef GPIO_InitStructure;
	GPIO_InitStructure.output_mode = GPIO_OUTPUT_PP;
	GPIO_InitStructure.pin_mode    = GPIO_PIN_OUT;
	GPIO_InitStructure.pin_speed   = GPIO_SPEED_HIGH;
	GPIO_InitStructure.pull_mode   = GPIO_PULL_NONE;
	GPIO_init( its->on_pins_[0], &GPIO_InitStructure );
	GPIO_init( its->on_pins_[1], &GPIO_InitStructure );
	GPIO_InitStructure.pin_mode = GPIO_PIN_IN;

	RCC_APB2PeriphClockCmd( RCC_APB2Periph_AFIO, ENABLE );

	EXTI_InitTypeDef EXTI_InitStructure;
	EXTI_InitStructure.EXTI_Mode	  = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;

	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0E;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority        = 0x0E;
	NVIC_InitStructure.NVIC_IRQChannelCmd                = ENABLE;

	for( uint8_t i = 0; i < 2; ++i )
	{
		GPIO_init( its->fault_pins_[i], &GPIO_InitStructure );

		GPIO_EXTILineConfig( its->fault_port_source_[i], its->fault_pin_source_[i] );

		EXTI_InitStructure.EXTI_Line = its->fault_exti_line_[i];
		EXTI_Init( &EXTI_InitStructure );

		NVIC_InitStructure.NVIC_IRQChannel = its->fault_irq_channel_[i];
		NVIC_Init( &NVIC_InitStructure );
	}

	ITS5215L_off( its, 0 );
	ITS5215L_off( its, 1 );

	DELAY_us( 500 );

	for( uint8_t i = 0; i < 2; ++i )
	{
		if( !GPIO_readInputDataBit( its->fault_pins_[i]) )
		{
			its->info_->faults_[i] = ITS5215_OPENLOAD;
		}
	}
}

void ITS5215L_on( const ITS5215L_MyDef* its, uint8_t ch )
{
	GPIO_writeBit( its->on_pins_[ch], 1 );
}

void ITS5215L_off( const ITS5215L_MyDef* its, uint8_t ch )
{
	GPIO_writeBit( its->on_pins_[ch], 0 );
}

void ITS5215L_toggle( const ITS5215L_MyDef* its, uint8_t ch )
{
	GPIO_toggleBit( its->on_pins_[ch] );
}

void ITS5215L_set( const ITS5215L_MyDef* its, uint8_t ch, uint8_t state )
{
	GPIO_writeBit( its->on_pins_[ch], state );
}

ITS5215L_Error ITS5215L_getFault( const ITS5215L_MyDef *its, uint8_t ch )
{
	return its->info_->faults_[ch];
}

void ITS5215L_clearFault( const ITS5215L_MyDef *its, uint8_t ch )
{
	its->info_->faults_[ch] = ITS5215_NO_ERROR;
}

void ITS5215L_irq_( const ITS5215L_MyDef *its, uint8_t ch )
{
	if( GPIO_readOutputDataBit( its->on_pins_[ch] ) )
		its->info_->faults_[ch] = ITS5215_OVERTEMP;
	else
		its->info_->faults_[ch] = ITS5215_OPENLOAD;
}
