/*
 * can.h
 *
 *  Created on: Apr 28, 2014
 *      Author: dave
 */

#ifndef __MODULES__MODULES_STM32FX__CAN_H__
#define __MODULES__MODULES_STM32FX__CAN_H__

#include "stm32fx_modules.h"
#include "can_defines_ibex.h"

#ifdef STM32F405XX
enum CAN_eBaudrate { BAUD_1M = 2, BAUD_500K = 4, BAUD_250K = 8, BAUD_125K = 16 };
#elif defined(STM32F103XX)
enum CAN_eBaudrate { BAUD_1M = 3, BAUD_500K = 6, BAUD_250K = 12, BAUD_125K = 24 };
#endif
//--------------------------------------------------------------------------------------------------
// This function lets you declare 2 filters. Short explanation:
// With id1/2 you specify a bitmask. Witch match_mask1/2 you specify which of the bits in id1/2
// *must* match and which are "don't care".
// Example:                id = 10101101
//                 match_mask = 01110011
//          received_messages = x010xx01 ,where x can be arbitrary
//
// @param CAN_TypeDef* canx     Specify if filter applies to CAN1 or CAN2
// @param uint16_t id1          Word to apply the matching mask (match_mask1) on
// @param uint16_t match_mask1  Bitmask which specifies which bits in id1 *must* match (where mask bits=1) and which are arbitrary (where mask bits=0)
// @param uint16_t id2          Word to apply the matching mask (match_mask2) on
// @param uint16_t match_mask2  Bitmask which specifies which bits in id2 *must* match (where mask bits=1) and which are arbitrary (where mask bits=0)
//
// @return 0 on success, -1 if no more free filter banks
//
int CAN_addMaskFilters16Bit( CAN_TypeDef* canx, uint16_t id1, uint16_t match_mask1, uint16_t id2, uint16_t match_mask2);

//--------------------------------------------------------------------------------------------------
// Specifies 4 ID filters. Only message ids which exactly match one of the filters are not dropped.
//
// @param CAN_TypeDef* canx     Specify if filter applies to CAN1 or CAN2
// @param uint16_t id1          First filter identifier
// @param uint16_t id2          Second filter identifier
// @param uint16_t id3          Third filter identifier
// @param uint16_t id4          Fourth filter identifier
//
// @return 0 on success, -1 if no more free filter banks
//
int CAN_addIdFilter16Bit( CAN_TypeDef* CANX, uint16_t id1, uint16_t id2, uint16_t id3, uint16_t id4);

//--------------------------------------------------------------------------------------------------
// Initializes the CAN with the given peripherals
//
// @param enum CAN_eBoardId board_id    Id of this board which is sent in all further communication
// @param CANRx_MyDef* can_rx           The CAN_RX peripheral to use
// @param CANTx_MyDef* can_tx           The CAN_TX peripheral to use
// @param uint8_t irq_priority          Interrupt priority of the CANx_RX and CANx_TX interrupts
// @param uint8_t irq_subpriority       Interrupt subpriority of the CANx_RX and CANx_TX interrupts
// @param enum CAN_eBaudrate baudrate   Baudrate to use on this CAN
//
// @return 0 on success, -1 on failure
//
int CAN_init(enum CAN_eBoardId board_id, const CANRx_MyDef* can_rx, const CANTx_MyDef* can_tx,
             uint8_t irq_priority, uint8_t irq_subpriority, enum CAN_eBaudrate baudrate);

//--------------------------------------------------------------------------------------------------
// Write data on specified CAN.
//
// @param CAN_TypeDef canx              The CAN peripheral to werite to. Either CAN1 or CAN2
// @param enum CAN_eMessageId msg_id    The id of the message to be sent
// @param uint8_t* data                 Pointer to the data bytes which will be sent
// @param uint8_t length_in_bytes       Number of data bytes to write
//
// @return 0 on success, -1 if no free mailbox
//
int CAN_writeData( CAN_TypeDef* canx, enum CAN_eMessageId msg_id, uint8_t* data, uint8_t length_in_bytes);

//--------------------------------------------------------------------------------------------------
// Write data on specified CAN. When data is not transmitted before timeout is reached, the
// timeout function will be called.
//
// @param CAN_TypeDef canx              The CAN peripheral to werite to. Either CAN1 or CAN2
// @param enum CAN_eMessageId msg_id    The id of the message to be sent
// @param uint8_t* data                 Pointer to the data bytes which will be sent
// @param uint8_t length_in_bytes       Number of data bytes to write
// @param uint8t_t timeout              Timeout in multiples of timeout timer period
// @param void (*timeout_function)()    The function to call if the timeout occurs
//
// @return 0 on success, -1 if no free mailbox
//
int CAN_writeDataTimeout( CAN_TypeDef* canx, enum CAN_eMessageId msg_id, uint8_t* data, uint8_t length_in_bytes, uint8_t timeout, void (*timeout_function)());

//--------------------------------------------------------------------------------------------------
// Write Remote Transmission Request on specified CAN.
//
// @param CAN_TypeDef canx              The CAN peripheral to werite to. Either CAN1 or CAN2
// @param enum CAN_eMessageId msg_id    The id of the message to be sent
//
// @return 0 on success, -1 if no free mailbox
//
int CAN_writeRemoteTransmissionRequest( CAN_TypeDef* canx, enum CAN_eMessageId msg_id);

//--------------------------------------------------------------------------------------------------
// Write Remote Transmission Request on specified CAN. When request is not transmitted before
// timeout is reached, the timeout function will be called.
// @param CAN_TypeDef canx              The CAN peripheral to werite to. Either CAN1 or CAN2
// @param enum CAN_eMessageId msg_id    The id of the message to be sent
// @param uint8t_t timeout              Timeout in multiples of timeout timer period
// @param void (*timeout_function)()    The function to call if the timeout occurs
// @return 0 on success, -1 if no free mailbox
//
int CAN_writeRemoteTransmissionRequestTimeout( CAN_TypeDef* canx, enum CAN_eMessageId msg_id, uint8_t timeout, void (*timeout_function)());

//--------------------------------------------------------------------------------------------------
// Read message from specified CAN.
//
// @param CAN_TypeDef* canx               read from CAN1 or CAN2
// @param enum CAN_eMessageId* msg_id     Storage address of message ID
// @param enum CAN_eBoardId* board_id     Storage address of board ID
// @param uint8_t* data                   Storage address of data. Attention: up to 8 bytes!
// @param uint8_t* data_length_in_bytes   Storage address of data length code (DLC)
// @return always 0
//
int CAN_read(CAN_TypeDef* canx, enum CAN_eMessageId* msg_id, enum CAN_eBoardId* board_id, uint8_t* data, uint8_t* data_length_in_bytes);

#endif // __MODULES__MODULES_STM32FX__CAN_H__

