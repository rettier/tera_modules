/*
 * ADC_DMA_config.h
 *
 *  Created on: Mar 4, 2014
 *      Author: dave
 */

#ifndef __MODULES__MODULES_STM32FX__ADC_DMA_H__
#define __MODULES__MODULES_STM32FX__ADC_DMA_H__

#include "stm32fx_modules.h"

uint16_t* ADC_initWithDMA(const ADC_MyDef* adcx_chy, const DMA_MyDef* dmax_streamy, uint32_t dma_channelx);

#endif // __MODULES__MODULES_STM32FX__ADC_DMA_H__
