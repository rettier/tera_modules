/*
 * stm32f4_def.h
 *
 *  Created on: Mar 23, 2014
 *      Author: dave
 */

#ifndef __MODULES__MODULES_STM32FX__STM32F4_DEF_H__
#define __MODULES__MODULES_STM32FX__STM32F4_DEF_H__

#ifdef STM32F405XX

#define HAS_CAN2

//-------------------------------------------------------------------------------------------------
// GPIO
//
typedef struct
{
  GPIO_TypeDef* gpiox_;
  uint16_t gpio_pin_x_;
  uint32_t rcc_ahb1periph_gpiox_;
} GPIO_MyDef;

void copyGPIO_Mydef(const GPIO_MyDef* from, GPIO_MyDef* to);

#define MY_GPIO_MACRO(port, pin) {GPIO##port, GPIO_Pin_##pin, RCC_AHB1Periph_GPIO##port}

#define NONE  {0,0,0}

//PA0 - PA15
#define PA0   MY_GPIO_MACRO(A,0)
#define PA1   MY_GPIO_MACRO(A,1)
#define PA2   MY_GPIO_MACRO(A,2)
#define PA3   MY_GPIO_MACRO(A,3)
#define PA4   MY_GPIO_MACRO(A,4)
#define PA5   MY_GPIO_MACRO(A,5)
#define PA6   MY_GPIO_MACRO(A,6)
#define PA7   MY_GPIO_MACRO(A,7)
#define PA8   MY_GPIO_MACRO(A,8)
#define PA9   MY_GPIO_MACRO(A,9)
#define PA10  MY_GPIO_MACRO(A,10)
#define PA11  MY_GPIO_MACRO(A,11)
#define PA12  MY_GPIO_MACRO(A,12)
#define PA13  MY_GPIO_MACRO(A,13)
#define PA14  MY_GPIO_MACRO(A,14)
#define PA15  MY_GPIO_MACRO(A,15)

//PB0 - PB15
#define PB0   MY_GPIO_MACRO(B,0)
#define PB1   MY_GPIO_MACRO(B,1)
#define PB2   MY_GPIO_MACRO(B,2)
#define PB3   MY_GPIO_MACRO(B,3)
#define PB4   MY_GPIO_MACRO(B,4)
#define PB5   MY_GPIO_MACRO(B,5)
#define PB6   MY_GPIO_MACRO(B,6)
#define PB7   MY_GPIO_MACRO(B,7)
#define PB8   MY_GPIO_MACRO(B,8)
#define PB9   MY_GPIO_MACRO(B,9)
#define PB10  MY_GPIO_MACRO(B,10)
#define PB11  MY_GPIO_MACRO(B,11)
#define PB12  MY_GPIO_MACRO(B,12)
#define PB13  MY_GPIO_MACRO(B,13)
#define PB14  MY_GPIO_MACRO(B,14)
#define PB15  MY_GPIO_MACRO(B,15)

//PC0 - PC15
#define PC0   MY_GPIO_MACRO(C,0)
#define PC1   MY_GPIO_MACRO(C,1)
#define PC2   MY_GPIO_MACRO(C,2)
#define PC3   MY_GPIO_MACRO(C,3)
#define PC4   MY_GPIO_MACRO(C,4)
#define PC5   MY_GPIO_MACRO(C,5)
#define PC6   MY_GPIO_MACRO(C,6)
#define PC7   MY_GPIO_MACRO(C,7)
#define PC8   MY_GPIO_MACRO(C,8)
#define PC9   MY_GPIO_MACRO(C,9)
#define PC10  MY_GPIO_MACRO(C,10)
#define PC11  MY_GPIO_MACRO(C,11)
#define PC12  MY_GPIO_MACRO(C,12)
#define PC13  MY_GPIO_MACRO(C,13)
#define PC14  MY_GPIO_MACRO(C,14)
#define PC15  MY_GPIO_MACRO(C,15)

//PD0 - PD15
//add more

//-------------------------------------------------------------------------------------------------
// Timer
//
typedef struct
{
  GPIO_MyDef gpio_struct_;
  TIM_TypeDef* timx_;
  uint8_t gpio_pinsourcex_;
  uint8_t gpio_af_timx_;
  uint32_t rcc_apbxperiph_timy_;
  IRQn_Type timx_irqn_;
  uint16_t tim_channel_x_;
} Timer_MyDef;

void copyTimer_Mydef(const Timer_MyDef* from, Timer_MyDef* to);

#define MY_ADV_TIMER_MACRO(timer, pin, rcc) TIM##timer, GPIO_PinSource##pin, GPIO_AF_TIM##timer, RCC_APB##rcc##Periph_TIM##timer
#define MY_TIMER_MACRO(timer, pin, rcc) TIM##timer, GPIO_PinSource##pin, GPIO_AF_TIM##timer, RCC_APB##rcc##Periph_TIM##timer, TIM##timer##_IRQn
#define MY_BASIC_TIMER_MACRO(timer, pin, rcc, irqn) TIM##timer, GPIO_PinSource##pin, 0, RCC_APB##rcc##Periph_TIM##timer, TIM##irqn##_IRQn

//Timer1, irqn can be a value of :
//TIM1_BRK_TIM9_IRQn       TIM1 Break interrupt and TIM9 global interrupt
//TIM1_UP_TIM10_IRQn       TIM1 Update Interrupt and TIM10 global interrupt
//TIM1_TRG_COM_TIM11_IRQn  TIM1 Trigger and Commutation Interrupt and TIM11 global interrupt
//TIM1_CC_IRQn             TIM1 Capture Compare Interrupt
#define TIM1_CH2_PA9(irqn)  {PA9, MY_ADV_TIMER_MACRO(1,9,2), irqn, 2}

// Timer2
#define TIM2_CH1_PA0  {PA0, MY_TIMER_MACRO(2,0,1), TIM_Channel_1}
#define TIM2_CH1_PA5  {PA5, MY_TIMER_MACRO(2,5,1), TIM_Channel_1}
#define TIM2_CH1_PA15 {PA15, MY_TIMER_MACRO(2,15,1), TIM_Channel_1}
#define TIM2_CH2_PA1  {PA1, MY_TIMER_MACRO(2,1,1), TIM_Channel_2}

// Timer3
#define TIM3_CH1_PA6 {PA6, MY_TIMER_MACRO(3,6,1), TIM_Channel_1}
#define TIM3_CH1_PB4 {PB4, MY_TIMER_MACRO(3,4,1), TIM_Channel_1}
#define TIM3_CH1_PC6 {PC6, MY_TIMER_MACRO(3,6,1), TIM_Channel_1}
#define TIM3_CH2_PA7 {PA7, MY_TIMER_MACRO(3,7,1), TIM_Channel_2}
#define TIM3_CH2_PB5 {PB5, MY_TIMER_MACRO(3,5,1), TIM_Channel_2}
#define TIM3_CH2_PC7 {PC7, MY_TIMER_MACRO(3,7,1), TIM_Channel_2}

//Timer4
#define TIM4_CH1_PB6 {PB6, MY_TIMER_MACRO(4,6,1), TIM_Channel_1}
#define TIM4_CH2_PB7 {PB7, MY_TIMER_MACRO(4,7,1), TIM_Channel_2}
#define TIM4_CH3_PB8 {PB8, MY_TIMER_MACRO(4,8,1), TIM_Channel_3}
#define TIM4_CH1_PB9 {PB9, MY_TIMER_MACRO(4,9,1), TIM_Channel_4}

//Timer5
#define TIM5_CH1_PA0 {PA0, MY_TIMER_MACRO(5,0,1), TIM_Channel_1}
#define TIM5_CH2_PA1 {PA1, MY_TIMER_MACRO(5,1,1), TIM_Channel_2}
#define TIM5_CH3_PA2 {PA2, MY_TIMER_MACRO(5,2,1), TIM_Channel_3}
#define TIM5_CH4_PA3 {PA3, MY_TIMER_MACRO(5,3,1), TIM_Channel_4}

//Timer6
#define TIM6_CH1     {NONE, MY_BASIC_TIMER_MACRO(6,0,1,6_DAC), TIM_Channel_1}
#define TIM7_CH1     {NONE, MY_BASIC_TIMER_MACRO(7,0,1,7), TIM_Channel_1}

//-------------------------------------------------------------------------------------------------
// ADC
typedef struct
{
  GPIO_MyDef gpio_struct_;
  ADC_TypeDef* adcx_;
  uint8_t adc_channel_x_;
  uint32_t rcc_apb2periph_adcx_;
} ADC_MyDef;

#define MY_ADC_MACRO(number, channel) ADC##number, ADC_Channel_##channel, RCC_APB2Periph_ADC##number

// ADC1
#define ADC1_CH10 {PC0, MY_ADC_MACRO(1, 10)}
#define ADC1_CH11 {PC1, MY_ADC_MACRO(1, 11)}
#define ADC1_CH12 {PC2, MY_ADC_MACRO(1, 12)}
#define ADC1_CH13 {PC3, MY_ADC_MACRO(1, 13)}

// ADC2
#define ADC2_CH10 {PC0, MY_ADC_MACRO(2, 10)}
#define ADC2_CH11 {PC1, MY_ADC_MACRO(2, 11)}
#define ADC2_CH12 {PC2, MY_ADC_MACRO(2, 12)}
#define ADC2_CH13 {PC3, MY_ADC_MACRO(2, 13)}

// ADC3
#define ADC3_CH10 {PC0, MY_ADC_MACRO(3, 10)}
#define ADC3_CH11 {PC1, MY_ADC_MACRO(3, 11)}
#define ADC3_CH12 {PC2, MY_ADC_MACRO(3, 12)}
#define ADC3_CH13 {PC3, MY_ADC_MACRO(3, 13)}

//-------------------------------------------------------------------------------------------------
// DMA
typedef struct
{
  DMA_Stream_TypeDef* dmax_streamy_;
  uint32_t rcc_ahb1periph_dmax_;
  IRQn_Type dmax_streamy_irq_;
} DMA_MyDef;
#define MY_DMA_MACRO(number, stream) {DMA##number##_Stream##stream, RCC_AHB1Periph_DMA##number, DMA##number##_Stream##stream##_IRQn}

// DMA2
#define DMA2_STREAM0 MY_DMA_MACRO(2,0)


//-------------------------------------------------------------------------------------------------
// CAN
typedef struct
{
  GPIO_MyDef gpio_struct_;
  uint8_t gpio_pinsource_;
  uint8_t gpio_af_canx_;         // GPIO_AF_CAN2 || GPIO_AF_CAN1
  CAN_TypeDef* canx_;
  uint32_t rcc_apb1periph_canx_; // (RCC_APB1Periph_CAN1 || RCC_APB1Periph_CAN2)
} CANTx_MyDef;

typedef struct
{
  GPIO_MyDef gpio_struct_;
  uint8_t gpio_pinsource_;
  uint8_t gpio_af_canx_;         // GPIO_AF_CAN2 || GPIO_AF_CAN1
  CAN_TypeDef* canx_;
  uint32_t rcc_apb1periph_canx_; // (RCC_APB1Periph_CAN1 || RCC_APB1Periph_CAN2)
} CANRx_MyDef;

#define MY_CAN_MACRO(pinsource, can_nr) GPIO_PinSource##pinsource, GPIO_AF_CAN##can_nr, CAN##can_nr, RCC_APB1Periph_CAN##can_nr

// CAN 1
#define CAN1_RX_PB8  { PB8, MY_CAN_MACRO(8, 1) }
#define CAN1_TX_PB9  { PB9, MY_CAN_MACRO(9, 1) }
#define CAN1_RX_PA11 { PA11, MY_CAN_MACRO(11, 1) }
#define CAN1_TX_PA12 { PA12, MY_CAN_MACRO(12, 1) }

//CAN 2
#define CAN2_RX_PB5  { PB5, MY_CAN_MACRO(5, 2) }
#define CAN2_TX_PB6  { PB6, MY_CAN_MACRO(6, 2) }
#define CAN2_RX_PB12 { PB12, MY_CAN_MACRO(12, 2) }
#define CAN2_TX_PB13 { PB13, MY_CAN_MACRO(13, 2) }

#endif // STM32F4XX

#endif // __MODULES__MODULES_STM32FX__STM32F4_DEF_H__
