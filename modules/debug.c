#include "debug.h"

void assert_failed( uint8_t* file, uint32_t line, uint8_t* str )
{
	LOG_ASSERT( "%s at: %s %i\n", str, file, (int)line );
	fflush( stdout );
	asm volatile( "bkpt 0" );
}
