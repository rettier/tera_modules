#ifndef __MODULES_ADC_DMA2_H__
#define __MODULES_ADC_DMA2_H__

#include "stm32fx_modules.h"

extern void ADC_init( const ADC_MyDef *adc );
extern void ADC_initChannel( const ADC_MyDef *adc, const ADC_Channel_MyDef* channel );

#endif // __MODULES_ADC_DMA2_H__

