#include "tle8104e.h"
#include "debug.h"
#include "delay.h"

uint8_t TLE8104E_init( const TLE8104E_MyDef *tle )
{
	tle->info_->diag_ 	         = 0xFF;
	tle->info_->channels_spi_    = 0x00;
	tle->info_->channels_input_  = 0x00;
	tle->info_->expect_diag_     = 0x00;
	tle->info_->fault_ 					 = 0x00;

	tle->rcc_clock_cmd_( tle->rcc_clock_, ENABLE );

	// configure spi
	SPI_InitTypeDef SPI_InitStructure;
	SPI_InitStructure.SPI_NSS 			    = SPI_NSS_Soft;
	SPI_InitStructure.SPI_Mode 		      = SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize      = SPI_DataSize_8b;
	SPI_InitStructure.SPI_Direction     = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_FirstBit      = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CPHA 		      = SPI_CPHA_2Edge;
	SPI_InitStructure.SPI_CPOL 		      = SPI_CPOL_Low;
	SPI_InitStructure.SPI_CRCPolynomial = 0x01;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;
	SPI_Init( tle->spi_, &SPI_InitStructure );
	SPI_CalculateCRC( tle->spi_, DISABLE );
	SPI_Cmd( tle->spi_, ENABLE );

	// configure pins
	// sck, mosi pp-output
	GPIO_MyInitDef GPIO_InitStructure;
	GPIO_InitStructure.output_mode = GPIO_OUTPUT_PP;
	GPIO_InitStructure.pin_mode    = GPIO_PIN_AF;
	GPIO_InitStructure.pin_speed   = GPIO_SPEED_HIGH;
	GPIO_InitStructure.pull_mode   = GPIO_PULL_NONE; // pull none is important for 5V tol.
	GPIO_init( tle->sck_pin_ , &GPIO_InitStructure );
	GPIO_init( tle->mosi_pin_, &GPIO_InitStructure );

	// miso, nfault input
	GPIO_InitStructure.pin_mode = GPIO_PIN_IN;
	GPIO_init( tle->miso_pin_ , &GPIO_InitStructure );
	GPIO_init( tle->fault_pin_, &GPIO_InitStructure );

	// cs and reset as od, since we are pulling 5V (100k pullup) down
	GPIO_InitStructure.output_mode = GPIO_OUTPUT_OD;
	GPIO_InitStructure.pin_mode    = GPIO_PIN_OUT;
	GPIO_init( tle->cs_pin_, &GPIO_InitStructure );
	GPIO_writeBit( tle->cs_pin_, 1 );

	GPIO_init( tle->reset_pin_, &GPIO_InitStructure );
	GPIO_writeBit( tle->reset_pin_, 1 );


	// initialize external interrupt
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_AFIO, ENABLE );

  GPIO_EXTILineConfig( tle->fault_port_source_, tle->fault_pin_source_ );

  EXTI_InitTypeDef EXTI_InitStructure;
  EXTI_InitStructure.EXTI_Line	  = tle->fault_exti_line_;
  EXTI_InitStructure.EXTI_Mode	  = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init( &EXTI_InitStructure );

  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_InitStructure.NVIC_IRQChannel 						   	   = tle->fault_irq_channel_;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority        = 0x0F;
  NVIC_InitStructure.NVIC_IRQChannelCmd                = ENABLE;
  NVIC_Init( &NVIC_InitStructure );

	TLE8104E_reset( tle );
	tle->info_->test_success_ = TLE8104E_test( tle );
	if( tle->info_->test_success_ )
	{
		TLE8104E_setChannelsAND( tle, 0x00 );
		TLE8104E_updateDiagnosis( tle );
	}else
	{
		LOG_ERROR( "tle8104e test failed" );
	}

	return tle->info_->test_success_;
}

void TLE8104E_reset( const TLE8104E_MyDef *tle )
{
	GPIO_writeBit( tle->reset_pin_, 0 );
	DELAY_us( 20 );
	GPIO_writeBit( tle->reset_pin_, 1 );
}

uint8_t TLE8104E_write( const TLE8104E_MyDef *tle, TLE8104E_Cmd msg )
{
	// pull ~CS low
	GPIO_writeBit( tle->cs_pin_, 0 );

	// send, wait for the end of the transmission and read back data
	while( SPI_I2S_GetFlagStatus( tle->spi_, SPI_I2S_FLAG_TXE ) == RESET );
  SPI_I2S_SendData( tle->spi_, msg );
  while( SPI_I2S_GetFlagStatus( tle->spi_, SPI_I2S_FLAG_BSY  ) != RESET );
  while( SPI_I2S_GetFlagStatus( tle->spi_, SPI_I2S_FLAG_RXNE ) != SET   );
  uint8_t res = SPI_I2S_ReceiveData( tle->spi_ );

  // pull the ~CS high for at least 10us
  GPIO_writeBit( tle->cs_pin_, 1 );
  DELAY_us( 10 );

  return res;
}

bool TLE8104E_test( const TLE8104E_MyDef *tle )
{
	// while the test routes back our command it also processes
	// it, so we use F0 (= set mode to AND, and AND all channels with 0)
	TLE8104E_write( tle, TLE8104E_CMD_ECHO );
	uint8_t res = TLE8104E_write( tle, 0xF0 );
	return res == 0xF0;
}

uint8_t TLE8104E_read( const TLE8104E_MyDef *tle )
{
	tle->info_->expect_diag_ = 1;
	return TLE8104E_write( tle, 0x00 );
}

uint8_t TLE8104E_hasFault( const TLE8104E_MyDef *tle )
{
	uint8_t res = tle->info_->fault_;
	return res;
}

void TLE8104E_clearFault( const TLE8104E_MyDef *tle )
{
	tle->info_->fault_ = 0;
}

uint8_t TLE8104E_updateDiagnosis( const TLE8104E_MyDef *tle )
{
	// if we expect std debug output (due to a CMD_BOL_*) we do
	// not need to send a diagnosis request
	if( !tle->info_->expect_diag_ )
	{
		TLE8104E_write( tle, TLE8104E_CMD_DIAGNOSIS );
		DELAY_us( 200 );
	}else if( GPIO_readInputDataBit( tle->fault_pin_ ) == 1 )
	{
		// we only need to wait if the fault pin is high to
		// make sure it has enough time to process the diag info
		// if the pin is low the diag info is already processed
		DELAY_us( 200 );
	}

	tle->info_->diag_ = TLE8104E_read( tle );
	return tle->info_->diag_;
}

uint8_t TLE8104E_updateInput( const TLE8104E_MyDef *tle )
{
	TLE8104E_write( tle, TLE8104E_CMD_INPUT );
	tle->info_->channels_input_ = TLE8104E_read( tle );
	return tle->info_->channels_input_;
}

TLE8104E_Diag TLE8104E_diagnosisForChannel( const TLE8104E_MyDef *tle, uint8_t ch )
{
	uint8_t diag = tle->info_->diag_;
	diag = diag >> (ch * 2);
	diag = diag & 0x3;
	return diag;
}

void TLE8104E_setChannels_( const TLE8104E_MyDef *tle, TLE8104E_Cmd cmd, uint8_t channels )
{
	channels = channels & 0xF;
	TLE8104E_write( tle, cmd | channels );
	tle->info_->channels_spi_ = channels;
	tle->info_->expect_diag_  = 1;
}

void TLE8104E_setChannel_( const TLE8104E_MyDef *tle, TLE8104E_Cmd cmd, uint8_t channel, uint8_t state )
{
	uint8_t channels = tle->info_->channels_spi_;
	if( state == 0 )
		channels &=~ (1<<channel);
	else
		channels |= (1<<channel);
	TLE8104E_setChannels_( tle, cmd, channels );
}

void TLE8104E_irq_( const TLE8104E_MyDef *tle )
{
	tle->info_->fault_ = 1;
}

void TLE8104E_setChannelsOR( const TLE8104E_MyDef *tle, uint8_t channels )
{
	TLE8104E_setChannels_( tle, TLE8104E_CMD_BOL_OR, channels );
}

void TLE8104E_setChannelOR( const TLE8104E_MyDef *tle, uint8_t channel, uint8_t state )
{
	TLE8104E_setChannel_( tle, TLE8104E_CMD_BOL_OR, channel, state );
}

void TLE8104E_setChannelsAND( const TLE8104E_MyDef *tle, uint8_t channels )
{
	TLE8104E_setChannels_( tle, TLE8104E_CMD_BOL_AND, channels );
}

void TLE8104E_setChannelAND( const TLE8104E_MyDef *tle, uint8_t channel, uint8_t state )
{
	TLE8104E_setChannel_( tle, TLE8104E_CMD_BOL_AND, channel, state );
}

void TLE8104E_setChannels( const TLE8104E_MyDef *tle, uint8_t channels )
{
	TLE8104E_setChannels_( tle, TLE8104E_CMD_BOL_LAST, channels );
}

void TLE8104E_setChannel( const TLE8104E_MyDef *tle, uint8_t channel, uint8_t state )
{
	TLE8104E_setChannel_( tle, TLE8104E_CMD_BOL_LAST, channel, state );
}


