/*
 * timer.h
 *
 *  Created on: Mar 23, 2014
 *      Author: dave
 */

#ifndef __MODULES__MODULES_STM32FX__TIMER_H__
#define __MODULES__MODULES_STM32FX__TIMER_H__

#include "stm32fx_modules.h"

//--------------------------------------------------------------------------------------------------
// Initializes a periodic occurring interrupt on the specified TIMx_IRQn.
// NOTE: Make sure to set irqn_ to the update value when using TIM1 or 8!
// @param const Timer_MyDef* tim   The structure containing all relevant initialization data, watch out for IRQn on TIM1 and 8!
// @param uint32_t frequency       The frequency of the interrupt event
// @param uint8_t priority         The priority of the interrupt request
// @param uint8_t subpriority      The subpriority of the interrupt request
//
void TIMER_initPeriodicInterrupt(const Timer_MyDef* tim, uint32_t frequency, uint8_t priority,
    uint8_t subpriority);

//--------------------------------------------------------------------------------------------------
// Initializes a PWM output signal with the specified frequency on the specified Timer/GPIO Pin.
// Initial duty cycle is 0.
// @param const Timer_MyDef* timer    The structure containing all relevant initialization data
// @param uint32_t frequency          The frequency of the PWM
// @param uint32_t duty_factor        The initial duty factor of the PWM. Is set to 0 if not given.
//
void TIMER_initPWMOutput(const Timer_MyDef* timer, uint32_t frequency);
void TIMER_initPWMOutput2(const Timer_MyDef* timer, uint32_t frequency, float duty_factor);

//--------------------------------------------------------------------------------------------------
// Changes the duty cycle of a previously initialized PWM output.
// @param const Timer_MyDef* tim  The struct specifying for which timer/channel the duty cycle shall change
// @param uint8_t percent         The new duty cycle in percent
//
void TIMER_setPWMOutputDutyCycle(const Timer_MyDef* tim, float percent);

//--------------------------------------------------------------------------------------------------
// Gets frequency and duty factor of the PWM output signal
// @param const Timer_MyDef* timer  The struct specifying for which timer/channel was initialized to produce the PWM
// @param float* frequency          The address where the frequency will be stored
// @param float* duty_cycle         The address where the duty_factor will be stored
//
void TIMER_getPWMOutputValues(Timer_MyDef* timer, float* frequency, float* duty_cycle);

//--------------------------------------------------------------------------------------------------
// The PWM input feature needs 2 capture compare registers (CCR). By default, CCRx is taken as the
// frequency capturing, interrupt triggering register, where x is the provided channel number.
// The second CC register is determined as follows:
// Channel 1 -> CC1 (frequency/master), CC2 (duty factor/slave)
// Channel 2 -> CC2 (frequency/master), CC1 (duty factor/slave)
// Channel 3 -> CC3 (frequency/master), CC4 (duty factor/slave)
// Channel 4 -> CC4 (frequency/master), CC3 (duty factor/slave)
//
// @param const Timer_MyDef* tiemr  The struct containing all relevant initialization data, watch out for irqn on TIM1 and 8!
// @param uint8_t irq_priority      The priority of the interrupt request
// @param uint8_t irq_subpriority   The subpriority of the interrupt request
//
void TIMER_initPWMInput(const Timer_MyDef* timer, uint8_t irq_priority, uint8_t irq_subpriority);

//--------------------------------------------------------------------------------------------------
// Computes frequency and duty factor of the PWM signal on the specified timer/channel/port and
// stores them at the given addresses.
//
// @param const Timer_MyDef* timer  The struct specifying for which timer/channel was initialized to capture the PWM
// @param float* frequency          The address where the frequency will be stored
// @param float* duty_cycle         The address where the duty_factor will be stored
//
void TIMER_getPWMInputValues(const Timer_MyDef* timer, float* frequency, float* duty_cycle);


//--------------------------------------------------------------------------------------------------
// ATTENTION! Not tested yet!
void TIMER_initInputCapture(const Timer_MyDef* timer, uint8_t irq_priority, uint8_t irq_subpriority);

#endif // __MODULES__MODULES_STM32FX__TIMER_H__
