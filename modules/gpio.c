#include "gpio.h"

void GPIO_structInit( GPIO_MyInitDef* init )
{
  init->output_mode = GPIO_OUTPUT_OD;
  init->pull_mode   = GPIO_PULL_DOWN;
  init->pin_mode    = GPIO_PIN_IN;
  init->pin_speed   = GPIO_SPEED_HIGH;
}

void GPIO_configureStdPeriph( GPIO_MyInitDef *my_init, GPIO_InitTypeDef *std_init )
#if defined(STM32F103XX)
{
  static GPIOMode_TypeDef gpio_modes[] =
  {
    GPIO_Mode_Out_OD,
    GPIO_Mode_Out_PP,
    GPIO_Mode_AF_OD,
    GPIO_Mode_AF_PP
  };

  // convert input, output and pin mode into the single F103 StdPeriph mode type
  GPIOMode_TypeDef gpio_mode = 0;
  if( my_init->pin_mode == GPIO_PIN_AIN )
  {
    gpio_mode = GPIO_Mode_AIN;
  }else
  {
    if( my_init->pin_mode == GPIO_PIN_IN )
    {
    	if( my_init->pull_mode == GPIO_PULL_NONE )
    		gpio_mode = GPIO_Mode_IN_FLOATING;
    	else
    		gpio_mode = my_init->pull_mode == GPIO_PULL_UP ? GPIO_Mode_IPU : GPIO_Mode_IPU;
    }else
    {
    	uint8_t mode_index = my_init->output_mode == GPIO_OUTPUT_PP;
    	mode_index += 2 * ( my_init->pin_mode - 1 );

    	gpio_mode = gpio_modes[mode_index];
    }
  }

  // convert speed to the stdperiph speed
  GPIOSpeed_TypeDef gpio_speed;
  switch( my_init->pin_speed )
  {
  case GPIO_SPEED_LOW:    gpio_speed = GPIO_Speed_2MHz;  break;
  case GPIO_SPEED_MEDIUM: gpio_speed = GPIO_Speed_10MHz; break;

  default:
  case GPIO_SPEED_FAST:
  case GPIO_SPEED_HIGH:
  	gpio_speed = GPIO_Speed_50MHz;
  	break;
  }

  std_init->GPIO_Mode  = gpio_mode;
  std_init->GPIO_Speed = gpio_speed;
}
#elif defined(STM32F405XX)
{
	// The enum values are compatible with the F405 enums
  std_init->GPIO_Mode  = my_init->pin_mode;
  std_init->GPIO_Speed = my_init->pin_speed;
  std_init->GPIO_OType = my_init->output_mode;
  std_init->GPIO_PuPd  = my_init->pull_mode;
}
#endif

void GPIO_init( const GPIO_MyDef* GPIOX, GPIO_MyInitDef* init )
{
	GPIOX->rcc_clock_cmd_( GPIOX->rcc_clock_, ENABLE );

  GPIO_InitTypeDef GPIO_InitStructure;
  GPIO_InitStructure.GPIO_Pin = GPIOX->gpio_pin_x_;
  GPIO_configureStdPeriph( init, &GPIO_InitStructure );
  GPIO_Init( GPIOX->gpiox_, &GPIO_InitStructure );
}

uint8_t GPIO_readInputDataBit( const GPIO_MyDef *pin )
{
	return GPIO_ReadInputDataBit( pin->gpiox_, pin->gpio_pin_x_ );
}

uint8_t GPIO_readOutputDataBit( const GPIO_MyDef *pin )
{
	return GPIO_ReadOutputDataBit( pin->gpiox_, pin->gpio_pin_x_ );
}

void GPIO_writeBit( const GPIO_MyDef *pin, BitAction BitVal )
{
	GPIO_WriteBit( pin->gpiox_, pin->gpio_pin_x_, BitVal );
}

void GPIO_toggleBit( const GPIO_MyDef *pin )
{
	uint8_t current = GPIO_ReadOutputDataBit( pin->gpiox_, pin->gpio_pin_x_ );
	GPIO_WriteBit( pin->gpiox_, pin->gpio_pin_x_, !current );
}
