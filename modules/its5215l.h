#ifndef __MODULES_ITS5215L_H__
#define __MODULES_ITS5215L_H__

#include "stm32fx_modules.h"

typedef enum
{
	ITS5215_NO_ERROR = 0,
	ITS5215_OVERTEMP = 1,
	ITS5215_OPENLOAD = 2
} ITS5215L_Error;

typedef struct _ITS5215L_Info_
{
	volatile ITS5215L_Error faults_[2];
} ITS5215L_Info;

typedef struct _ITS5215L_MyDef_
{
	const GPIO_MyDef *fault_pins_[2];
	const GPIO_MyDef *on_pins_[2];

	uint8_t fault_pin_source_[2];
	uint8_t fault_port_source_[2];
	uint32_t fault_exti_line_[2];
	uint8_t fault_irq_channel_[2];

	ITS5215L_Info * const info_;

} ITS5215L_MyDef;

extern void ITS5215L_irq_( const ITS5215L_MyDef *its, uint8_t ch );

extern void ITS5215L_init( const ITS5215L_MyDef* its );
extern void ITS5215L_on( const ITS5215L_MyDef* its, uint8_t ch );
extern void ITS5215L_off( const ITS5215L_MyDef* its, uint8_t ch );
extern void ITS5215L_toggle( const ITS5215L_MyDef* its, uint8_t ch );
extern void ITS5215L_set( const ITS5215L_MyDef* its, uint8_t ch, uint8_t state );
extern ITS5215L_Error ITS5215L_getFault( const ITS5215L_MyDef* its, uint8_t ch );
extern void ITS5215L_clearFault( const ITS5215L_MyDef *its, uint8_t ch );

#endif // __MODULES_ITS5215L_H__
