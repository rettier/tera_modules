#ifndef __MODULES_TL8104E_H__
#define __MODULES_TL8104E_H__

#include "stm32fx_modules.h"
#include "gpio.h"

typedef enum
{
	TLE8104E_CMD_DIAGNOSIS = 0x00,
	TLE8104E_CMD_INPUT     = 0xC0,
	TLE8104E_CMD_ECHO      = 0xA0,
	TLE8104E_CMD_BOL_OR    = 0x30,
	TLE8104E_CMD_BOL_LAST  = 0xE0,
	TLE8104E_CMD_BOL_AND   = 0xF0
} TLE8104E_Cmd;

typedef enum
{
	TLE8104E_DIAG_SHORT  = 0x00,
	TLE8104E_DIAG_OPEN	 = 0x01,
	TLE8104E_DIAG_OVER	 = 0x02,
	TLE8104E_DIAG_NORMAL = 0x03
} TLE8104E_Diag;

typedef struct _TLE8104EInfo_MyDef_
{
	TLE8104E_Diag diag_;

	uint8_t channels_spi_   : 4;
	uint8_t channels_input_ : 4;

	uint8_t test_success_ : 1;
	uint8_t expect_diag_  : 1;

	volatile uint8_t fault_;
} TLE8104EInfo_MyDef;

typedef struct _TLE8104E_MyDef_
{
	const GPIO_MyDef *cs_pin_;
	const GPIO_MyDef *miso_pin_;
	const GPIO_MyDef *mosi_pin_;
	const GPIO_MyDef *sck_pin_;
	const GPIO_MyDef *reset_pin_;
	const GPIO_MyDef *fault_pin_;

	uint8_t fault_pin_source_;
	uint8_t fault_port_source_;
	uint32_t fault_exti_line_;
	uint8_t fault_irq_channel_;

	SPI_TypeDef * const spi_;
	TLE8104EInfo_MyDef * const info_;

	RCC_ClockCmdFunction rcc_clock_cmd_;
	uint32_t rcc_clock_;

} TLE8104E_MyDef;

extern void TLE8104E_irq_( const TLE8104E_MyDef *tle );

extern uint8_t TLE8104E_init( const TLE8104E_MyDef *tle );
extern void TLE8104E_reset( const TLE8104E_MyDef *tle );
extern bool TLE8104E_test( const TLE8104E_MyDef *tle );
extern uint8_t TLE8104E_updateDiagnosis( const TLE8104E_MyDef *tle );
extern TLE8104E_Diag TLE8104E_diagnosisForChannel( const TLE8104E_MyDef *tle, uint8_t ch );

extern uint8_t TLE8104E_hasFault( const TLE8104E_MyDef *tle );
extern void TLE8104E_clearFault( const TLE8104E_MyDef *tle );

extern void TLE8104E_setChannelsOR( const TLE8104E_MyDef *tle, uint8_t channels );
extern void TLE8104E_setChannelOR( const TLE8104E_MyDef *tle, uint8_t channel, uint8_t state );
extern void TLE8104E_setChannelsAND( const TLE8104E_MyDef *tle, uint8_t channels );
extern void TLE8104E_setChannelAND( const TLE8104E_MyDef *tle, uint8_t channel, uint8_t state );
extern void TLE8104E_setChannels( const TLE8104E_MyDef *tle, uint8_t channels );
extern void TLE8104E_setChannel( const TLE8104E_MyDef *tle, uint8_t channel, uint8_t state );

#endif // __MODULES_TL8104E_H__
