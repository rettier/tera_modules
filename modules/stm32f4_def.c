/*
 * stm32f4_def.c
 *
 *  Created on: Apr 15, 2014
 *      Author: dave
 */

#include "stm32f4_def.h"

#if 0

void copyGPIO_Mydef(const GPIO_MyDef* from, GPIO_MyDef* to)
{
  to->gpiox_ = from->gpiox_;
  to->gpio_pin_x_ = from->gpio_pin_x_;
  to->rcc_ahb1periph_gpiox_ = from->rcc_ahb1periph_gpiox_;
}


void copyTimer_Mydef(const Timer_MyDef* from, Timer_MyDef* to)
{
  copyGPIO_Mydef(&from->gpio_struct_, &to->gpio_struct_);

  to->timx_ = from->timx_;
  to->gpio_pinsourcex_ = from->gpio_pinsourcex_;
  to->gpio_af_timx_ = from->gpio_af_timx_;
  to->rcc_apbxperiph_timy_ = from->rcc_apbxperiph_timy_;
  to->timx_irqn_ = from->timx_irqn_;
  to->tim_channel_x_ = from->tim_channel_x_;
}

#endif
