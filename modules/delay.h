#ifndef __MODULES_DELAY_H__
#define __MODULES_DELAY_H__

#include "stm32fx_modules.h"

extern void DELAY_init( );
extern void DELAY_ms( uint32_t ms );
extern void DELAY_us( uint32_t us );

#endif //__MODULES_DELAY_H__
