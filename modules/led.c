#include "led.h"
#include "gpio.h"

extern const Led_MyDef * const LED_leds_[LED_COUNT];

void LED_initLed( const Led_MyDef* led )
{
	GPIO_MyInitDef GPIO_InitStructure;
	GPIO_InitStructure.output_mode = GPIO_OUTPUT_PP;
	GPIO_InitStructure.pin_mode    = GPIO_PIN_OUT;
	GPIO_InitStructure.pin_speed   = GPIO_SPEED_HIGH;
	GPIO_InitStructure.pull_mode   = GPIO_PULL_NONE;
	GPIO_init( led->gpio_struct_, &GPIO_InitStructure );

	GPIO_writeBit( led->gpio_struct_, !led->on_ );

}

void LED_init( )
{
	for( uint8_t i = 0; i < LED_COUNT; ++i )
		LED_initLed( LED_leds_[i] );
}

void LED_on( const Led_MyDef* led )
{
	GPIO_writeBit( led->gpio_struct_, led->on_ );
}

void LED_off( const Led_MyDef* led )
{
	GPIO_writeBit( led->gpio_struct_, !led->on_ );
}

void LED_toggle( const Led_MyDef *led )
{
	GPIO_toggleBit( led->gpio_struct_ );
}

void LED_set( const Led_MyDef *led, uint8_t val )
{
	GPIO_writeBit( led->gpio_struct_, val == led->on_ );
}
