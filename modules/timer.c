/*
 * timer.c
 *
 *  Created on: Mar 24, 2014
 *      Author: dave
 */

#include "timer.h"

#include "gpio.h"
#include "debug.h"

uint32_t getTimerFrequency(const Timer_MyDef* tim)
{
  //is there an easier/safer way?

  RCC_ClocksTypeDef rcc_clocks;
  RCC_GetClocksFreq(&rcc_clocks);

  uint32_t frequency = 0;
  if( IS_RCC_APB1_PERIPH( tim->rcc_clock_ ) )
  {
    frequency =
        rcc_clocks.PCLK1_Frequency / 4 <= rcc_clocks.HCLK_Frequency ?
            rcc_clocks.PCLK1_Frequency * 2 : rcc_clocks.PCLK1_Frequency;
  }
  else if( IS_RCC_APB2_PERIPH( tim->rcc_clock_ ) )
  {
    frequency =
        rcc_clocks.PCLK2_Frequency / 2 <= rcc_clocks.HCLK_Frequency ?
            rcc_clocks.PCLK2_Frequency * 2 : rcc_clocks.PCLK1_Frequency;
  }
  else
  {
  	assert(false);
  }

  return frequency;
}

void setupTimeBase(const Timer_MyDef* tim, uint32_t frequency)
{
  uint32_t timer_periph_freq = getTimerFrequency(tim);
  if (frequency > timer_periph_freq / 2)
      assert(false); // error, frequency too high

  uint32_t max_period_reg_value;
#ifdef STM32F405XX
  max_period_reg_value = (tim->timx_ == TIM2 || tim->timx_ == TIM5) ? 0xFFFFFFFF : 0xFFFF;
#else
  max_period_reg_value = 0xFFFF;
#endif
  uint32_t max_prescale_reg_value = 0x0000FFFF;

  uint32_t theoretical_period = (timer_periph_freq / frequency) - 1;
  uint32_t theoretical_prescaler = theoretical_period / max_period_reg_value;
  uint8_t clock_divisior = theoretical_prescaler / max_prescale_reg_value;

  if (clock_divisior && (tim->timx_ == TIM6 || tim->timx_ == TIM7))
  {
    assert(false); //error, too large period - Basic timers can't do clock division
  }

  uint16_t clock_divisior_def = TIM_CKD_DIV1;
  switch (clock_divisior)
  {
  case 0:
    clock_divisior = 1;
    clock_divisior_def = TIM_CKD_DIV1;
    break;
  case 1:
    clock_divisior = 2;
    clock_divisior_def = TIM_CKD_DIV2;
    break;
  case 2:
  case 3:
    clock_divisior = 4;
    clock_divisior_def = TIM_CKD_DIV4;
    break;
  default:
    assert(false); //error, too large period
  }

  uint32_t prescaler = theoretical_prescaler / clock_divisior;
  uint32_t period = (theoretical_period + 1) / (prescaler + 1) - 1;

  // Time base configuration
  TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
  TIM_TimeBaseStructure.TIM_Period = period;
  TIM_TimeBaseStructure.TIM_Prescaler = prescaler;
  TIM_TimeBaseStructure.TIM_ClockDivision = clock_divisior_def;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseInit(tim->timx_, &TIM_TimeBaseStructure);
}

void TIMER_initPeriodicInterrupt(const Timer_MyDef* timer, uint32_t frequency, uint8_t irq_priority,
                                 uint8_t irq_subpriority)
{
	timer->rcc_clock_cmd_( timer->rcc_clock_, ENABLE );

  //Enable periodic interrupt
  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_InitStructure.NVIC_IRQChannel = timer->timx_irqn_;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = irq_priority;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = irq_subpriority;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  setupTimeBase(timer, frequency);

  TIM_ITConfig(timer->timx_, TIM_IT_Update, ENABLE);

  // enable counter
  TIM_Cmd(timer->timx_, ENABLE);
}

void TIMER_initPWMOutput(const Timer_MyDef* timer, uint32_t frequency)
{
	TIMER_initPWMOutput2( timer, frequency, 0 );
}

void TIMER_initPWMOutput2(const Timer_MyDef* timer, uint32_t frequency, float duty_factor)
{
//  TIM_Cmd(timer->timx_, DISABLE);

	timer->rcc_clock_cmd_( timer->rcc_clock_, ENABLE );

  GPIO_MyInitDef GPIO_InitStructure;
  GPIO_InitStructure.pin_mode    = GPIO_PIN_AF;
  GPIO_InitStructure.output_mode = GPIO_OUTPUT_PP;
  GPIO_InitStructure.pull_mode   = GPIO_PULL_UP;
  GPIO_InitStructure.pin_speed   = GPIO_SPEED_HIGH;
  GPIO_init( timer->gpio_struct_, &GPIO_InitStructure );

#ifdef STM32F405XX
  GPIO_PinAFConfig(timer->gpio_struct_.gpiox_, timer->gpio_pinsourcex_, timer->gpio_af_timx_);
#endif

  setupTimeBase(timer, frequency);

  TIM_OCInitTypeDef TIM_OCInitStructure;
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_Pulse = timer->timx_->ARR / (float)100.0 * duty_factor;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

  switch (timer->tim_channel_x_)
  {
  case TIM_Channel_1:
    TIM_OC1Init(timer->timx_, &TIM_OCInitStructure);
    TIM_OC1PreloadConfig(timer->timx_, TIM_OCPreload_Enable);
    break;
  case TIM_Channel_2:
    TIM_OC2Init(timer->timx_, &TIM_OCInitStructure);
    TIM_OC2PreloadConfig(timer->timx_, TIM_OCPreload_Enable);
    break;
  case TIM_Channel_3:
    TIM_OC3Init(timer->timx_, &TIM_OCInitStructure);
    TIM_OC3PreloadConfig(timer->timx_, TIM_OCPreload_Enable);
    break;
  case TIM_Channel_4:
    TIM_OC4Init(timer->timx_, &TIM_OCInitStructure);
    TIM_OC4PreloadConfig(timer->timx_, TIM_OCPreload_Enable);
    break;
  default:
    assert(false); //error
  }

  TIM_ARRPreloadConfig(timer->timx_, ENABLE);
  TIM_Cmd(timer->timx_, ENABLE);
}

void TIMER_setPWMOutputDutyCycle(const Timer_MyDef* timer, float percent)
{
  switch (timer->tim_channel_x_)
  {
  case TIM_Channel_1:
    timer->timx_->CCR1 = timer->timx_->ARR / (float)100 * percent;
    break;
  case TIM_Channel_2:
    timer->timx_->CCR2 = timer->timx_->ARR / (float)100 * percent;
    break;
  case TIM_Channel_3:
    timer->timx_->CCR3 = timer->timx_->ARR / (float)100 * percent;
    break;
  case TIM_Channel_4:
    timer->timx_->CCR4 = timer->timx_->ARR / (float)100 * percent;
    break;
  default:
    assert(false); //error
  }
}

void TIMER_getPWMOutputValues(Timer_MyDef* timer, float* frequency, float* duty_cycle)
{
  uint32_t ccr = 0;
  uint32_t arr = timer->timx_->ARR;
  switch (timer->tim_channel_x_)
  {
  case TIM_Channel_1:
    ccr = timer->timx_->CCR1;
    break;
  case TIM_Channel_2:
    ccr = timer->timx_->CCR2;
    break;
  case TIM_Channel_3:
    ccr = timer->timx_->CCR3;
    break;
  case TIM_Channel_4:
    ccr = timer->timx_->CCR4;
    break;
  default:
    assert(false); //error
  }

  *frequency = getTimerFrequency(timer) / (float)((timer->timx_->PSC + 1) * arr);
  *duty_cycle = (float) ccr / (float) arr * 100;
}

void TIMER_initPWMInput(const Timer_MyDef* timer, uint8_t irq_priority, uint8_t irq_subpriority)
{
  if (timer->tim_channel_x_ != TIM_Channel_1 && timer->tim_channel_x_ != TIM_Channel_2)
    assert(false); //error, only possible with channel 1 and 2

  timer->rcc_clock_cmd_( timer->rcc_clock_, ENABLE );

  GPIO_MyInitDef GPIO_InitStructure;
  GPIO_InitStructure.pin_mode    = GPIO_PIN_AF_INPUT;
  GPIO_InitStructure.output_mode = GPIO_OUTPUT_PP;
  GPIO_InitStructure.pull_mode   = GPIO_PULL_NONE;
  GPIO_InitStructure.pin_speed   = GPIO_SPEED_FAST;
  GPIO_init( timer->gpio_struct_, &GPIO_InitStructure );

#ifdef STM32F405XX
  GPIO_PinAFConfig(timer->gpio_struct_.gpiox_, timer->gpio_pinsourcex_, timer->gpio_af_timx_);
#endif

  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_InitStructure.NVIC_IRQChannel = timer->timx_irqn_;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = irq_priority;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = irq_subpriority;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  TIM_ICInitTypeDef TIM_ICInitStructure;
  TIM_ICInitStructure.TIM_Channel = timer->tim_channel_x_;
  TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;
  TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
  TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
  TIM_ICInitStructure.TIM_ICFilter = 0x3;
  TIM_PWMIConfig(timer->timx_, &TIM_ICInitStructure);

  if (timer->tim_channel_x_ == TIM_Channel_1)
    TIM_SelectInputTrigger(timer->timx_, TIM_TS_TI1FP1);
  else
    TIM_SelectInputTrigger(timer->timx_, TIM_TS_TI2FP2);

  // Select the slave Mode: Reset Mode
  TIM_SelectSlaveMode(timer->timx_, TIM_SlaveMode_Reset);
  TIM_SelectMasterSlaveMode(timer->timx_, TIM_MasterSlaveMode_Enable);

  // TIM enable counter
  TIM_Cmd(timer->timx_, ENABLE);

  // Enable the CC Interrupt Request
  if (timer->tim_channel_x_ == TIM_Channel_1)
    TIM_ITConfig(timer->timx_, TIM_IT_CC1, ENABLE);
  else
    TIM_ITConfig(timer->timx_, TIM_IT_CC2, ENABLE);
}

void TIMER_getPWMInputValues(const Timer_MyDef* timer, float* frequency, float* duty_cycle)
{
  float ccr1 = TIM_GetCapture1(timer->timx_);
  float ccr2 = TIM_GetCapture2(timer->timx_);
  if (timer->tim_channel_x_ == TIM_Channel_1 && ccr1)
  {
    *frequency = getTimerFrequency(timer) / ccr1;
    *duty_cycle = ccr2 * 100 / ccr1;
  }
  else if (timer->tim_channel_x_ == TIM_Channel_2 && ccr2)
  {
    *frequency = getTimerFrequency(timer) / ccr2;
    *duty_cycle = ccr1 * 100 / ccr2;
  }
}

void TIMER_initInputCapture(const Timer_MyDef* timer, uint8_t irq_priority, uint8_t irq_subpriority)
{
	timer->rcc_clock_cmd_( timer->rcc_clock_, ENABLE );

  GPIO_MyInitDef GPIO_InitStructure;
  GPIO_InitStructure.pin_mode    = GPIO_PIN_AF_INPUT;
  GPIO_InitStructure.output_mode = GPIO_OUTPUT_PP;
  GPIO_InitStructure.pull_mode   = GPIO_PULL_NONE;
  GPIO_InitStructure.pin_speed   = GPIO_SPEED_FAST;
  GPIO_init( timer->gpio_struct_, &GPIO_InitStructure );

#ifdef STM32F405XX
  GPIO_PinAFConfig(timer->gpio_struct_.gpiox_, timer->gpio_pinsourcex_, timer->gpio_af_timx_);
#endif

  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_InitStructure.NVIC_IRQChannel = timer->timx_irqn_;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = irq_priority;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = irq_subpriority;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  TIM_ICInitTypeDef TIM_ICInitStructure;
  TIM_ICInitStructure.TIM_Channel = timer->tim_channel_x_;
  TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;
  TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
  TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
  TIM_ICInitStructure.TIM_ICFilter = 0x3;
  TIM_ICInit(timer->timx_, &TIM_ICInitStructure);

//  if (timer->tim_channel_x_ == TIM_Channel_1)
//    TIM_SelectInputTrigger(timer->timx_, TIM_TS_TI1FP1);
//  else
//    TIM_SelectInputTrigger(timer->timx_, TIM_TS_TI2FP2);

  // TIM enable counter
  TIM_Cmd(timer->timx_, ENABLE);

  // Enable the CC Interrupt Request
  if (timer->tim_channel_x_ == TIM_Channel_1)
    TIM_ITConfig(timer->timx_, TIM_IT_CC1, ENABLE);
  else
    TIM_ITConfig(timer->timx_, TIM_IT_CC2, ENABLE);
}

