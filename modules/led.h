#ifndef __MODULES_LED_H__
#define __MODULES_LED_H__

#include "stm32fx_modules.h"

extern void LED_init( );
extern void LED_initLed( const Led_MyDef* led );
extern void LED_on( const Led_MyDef* led );
extern void LED_off( const Led_MyDef* led );
extern void LED_toggle( const Led_MyDef *led );
extern void LED_set( const Led_MyDef *led, uint8_t val );

#endif // __MODULES_LED_H__
