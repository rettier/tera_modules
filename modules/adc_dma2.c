#include "adc_dma2.h"
#include "debug.h"
#include "gpio.h"

void ADC_processIRQ( const ADC_MyDef* adc )
{
  if( DMA_GetITStatus( adc->dma_->dma_it_tc_ ) )
  {
    DMA_ClearITPendingBit( adc->dma_->dma_it_tc_ );

    unsigned i = 0;
    ADC_Info_MyDef *info = adc->info_;
    for (i = 0; i < ADC_MAX_CHANNELS; i++)
    {
      info->sum_storage_[i] += info->temp_storage_[i];
    }

    static uint32_t buffercnt = 0;
    buffercnt = (buffercnt + 1) % ADC_BUFFERSIZE;
    if (buffercnt == 0)
    {
      for (i = 0; i < ADC_MAX_CHANNELS; i++)
      {
        info->avg_storage_[i] = info->sum_storage_[i] / ADC_BUFFERSIZE;
        info->sum_storage_[i] = 0;
      }

      if( adc->info_->avg_hook_ )
      	adc->info_->avg_hook_( adc );
    }
  }
}

#if ADC_MODULE_COUNT >= 1
void ADC1_DMA_IRQHandler( ){ ADC_processIRQ( ADC_1 ); }
#endif

#if ADC_MODULE_COUNT >= 2 && defined(ADC_2_INIT)
void ADC2_DMA_IRQHandler( ){ ADC_processIRQ( ADC_2 ); }
#endif

#if ADC_MODULE_COUNT >= 3
void ADC3_DMA_IRQHandler( ){ ADC_processIRQ( ADC_3 ); }
#endif

void ADC_initGPIO( const ADC_Channel_MyDef *ch )
{
	GPIO_MyInitDef GPIO_InitStructure;
	GPIO_InitStructure.output_mode = GPIO_OUTPUT_OD;
	GPIO_InitStructure.pin_mode    = GPIO_PIN_AIN;
	GPIO_InitStructure.pull_mode   = GPIO_PULL_NONE;
	GPIO_InitStructure.pin_speed   = GPIO_SPEED_HIGH;
	GPIO_init( ch->gpio_struct_, &GPIO_InitStructure );
}

void ADC_initDMA( const ADC_MyDef *adc, const DMA_MyDef *dma )
{
	dma->rcc_clock_cmd_( dma->rcc_clock_, ENABLE );

	DMA_InitTypeDef DMA_InitStructure;
  DMA_ITConfig( dma->dmax_channely_, DMA_IT_TC, DISABLE );
  DMA_DeInit( dma->dmax_channely_ );

  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&(adc->adcx_->DR);
  DMA_InitStructure.DMA_MemoryBaseAddr     = (uint32_t)adc->info_->temp_storage_;

  DMA_InitStructure.DMA_DIR 	     = DMA_DIR_PeripheralSRC;
  DMA_InitStructure.DMA_BufferSize = adc->info_->channels_initialized_;

  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc 	  = DMA_MemoryInc_Enable;

  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord; // ADC -> HalfWords
  DMA_InitStructure.DMA_MemoryDataSize 	   = DMA_MemoryDataSize_HalfWord;

  DMA_InitStructure.DMA_Mode 	   = DMA_Mode_Circular; //no reinit after 1 transfer necessary
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  DMA_InitStructure.DMA_M2M			 = DMA_M2M_Disable;

#ifdef STM32405XX
  DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
  DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull; //Will be ignored in direct mode
  DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single; //no burst transfers allowed in direct mode
  DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
#endif

  DMA_Init( dma->dmax_channely_, &DMA_InitStructure);
  DMA_Cmd( dma->dmax_channely_, ENABLE);

  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_InitStructure.NVIC_IRQChannel = dma->dmax_streamy_irq_;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  DMA_ITConfig( dma->dmax_channely_, DMA_IT_TC, ENABLE );
}

void ADC_initADC( const ADC_MyDef *adc, const ADC_Channel_MyDef* channel )
{
	ADC_InitTypeDef ADC_InitStructure;
	ADC_InitStructure.ADC_Mode 						   = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode       = ENABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
	ADC_InitStructure.ADC_ExternalTrigConv   = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign          = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel       = adc->info_->channels_initialized_;
	ADC_Init( adc->adcx_, &ADC_InitStructure );

  ADC_RegularChannelConfig(adc->adcx_, channel->adc_channel_x_,
  		adc->info_->channels_initialized_, ADC_SampleTime_239Cycles5 );

	ADC_DMACmd( adc->adcx_, ENABLE );
	ADC_Cmd( adc->adcx_, ENABLE );

	ADC_ResetCalibration( adc->adcx_ );
	while( ADC_GetResetCalibrationStatus( adc->adcx_ ) );
	ADC_StartCalibration( adc->adcx_ );
	while( ADC_GetCalibrationStatus( adc->adcx_ ) );

	ADC_SoftwareStartConvCmd( adc->adcx_, ENABLE );
}

void ADC_init( const ADC_MyDef *adc )
{
	ADC_Info_MyDef *info = adc->info_;
	info->channels_initialized_ = 0;

	for( uint8_t i = 0; i < ADC_MAX_CHANNELS; ++i )
	{
		info->avg_storage_ [i] = 0;
		info->temp_storage_[i] = 0;
		info->sum_storage_ [i] = 0;
	}
}

void ADC_initChannel( const ADC_MyDef *adc, const ADC_Channel_MyDef* channel )
{
	// init clocks
	RCC_ADCCLKConfig( ADC_CLOCK );
	adc->rcc_clock_cmd_( adc->rcc_clock_, ENABLE );

	adc->info_->channels_initialized_++;
	ADC_initGPIO( channel );
	ADC_initDMA( adc, adc->dma_ );
	ADC_initADC( adc, channel );
}
