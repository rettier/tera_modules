#include "delay.h"

void DELAY_init( )
{

}

void DELAY_ms( uint32_t us )
{

}

#define __NOP asm volatile( "NOP" );
#define __NOP2  __NOP  __NOP
#define __NOP4  __NOP2 __NOP2
#define __NOP8  __NOP4 __NOP4
#define __NOP16 __NOP8 __NOP8

// ghetto delay, please fix this
void DELAY_us( uint32_t us )
{
	for( uint32_t i = 0; i < us; ++i )
	{
		__NOP16;
		__NOP16;
		__NOP16;
		__NOP16;
	}
}
