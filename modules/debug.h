#ifndef __MODULES_DEBUG_H__
#define __MODULES_DEBUG_H__

#include "stm32fx_modules.h"
#include <stdio.h>

#if defined(USE_COLOR_DEBUGGING)
#define ANSI_COLOR_RESET 			"\x1B[0m"  // reset; clears all colors and styles (to white on black)
#define	ANSI_FG_COLOR_BLACK 	"\x1B[30m" // set foreground color to black
#define	ANSI_FG_COLOR_RED  		"\x1B[31m" // set foreground color to red
#define	ANSI_FG_COLOR_GREEN  	"\x1B[32m" // set foreground color to green
#define	ANSI_FG_COLOR_YELLOW 	"\x1B[33m" // set foreground color to yellow
#define	ANSI_FG_COLOR_BLUE 		"\x1B[34m" // set foreground color to blue
#define	ANSI_FG_COLOR_MAGENTA "\x1B[35m" // set foreground color to magenta (purple)
#define	ANSI_FG_COLOR_CYAN 	  "\x1B[36m" // set foreground color to cyan
#define	ANSI_FG_COLOR_WHITE 	"\x1B[37m" // set foreground color to white
#define	ANSI_FG_COLOR_DEFAULT "\x1B[39m" // set foreground color to default (white)
#else
#define ANSI_COLOR_RESET 			""
#define	ANSI_FG_COLOR_BLACK   ""
#define	ANSI_FG_COLOR_RED     ""
#define	ANSI_FG_COLOR_GREEN   ""
#define	ANSI_FG_COLOR_YELLOW  ""
#define	ANSI_FG_COLOR_BLUE    ""
#define	ANSI_FG_COLOR_MAGENTA ""
#define	ANSI_FG_COLOR_CYAN    ""
#define	ANSI_FG_COLOR_WHITE   ""
#define	ANSI_FG_COLOR_DEFAULT ""
#endif

#define LOG_LF "\n"

#ifdef DEBUG
#	define LOG( color, tag, x, ... ) printf( color "[ " tag " ] - " x ANSI_COLOR_RESET LOG_LF, ##__VA_ARGS__ )
#else
#	define LOG( color, tag, x, ... )
#endif

extern void assert_failed(uint8_t* file, uint32_t line, uint8_t* str);

#ifdef USE_ASSERT
#	define assert(expr) assert_failed( (uint8_t*)__FILE__, __LINE__, (uint8_t*)(#expr "failed ") )
# define assert2(str) assert_failed( (uint8_t*)__FILE__, __LINE__, (uint8_t*)str )
#else
#	define assert(expr)
#	define assert2(str)
#endif

#ifdef DEBUG_LOG_INFO
#	define LOG_INFO( x, ... ) LOG(ANSI_FG_COLOR_DEFAULT, "INFO  ", x, ##__VA_ARGS__ )
#else
#	define LOG_INFO( x, ... )
#endif

#ifdef DEBUG_LOG_WARNING
#define LOG_WARN( x, ... ) LOG(ANSI_FG_COLOR_YELLOW, "WARN  ", x, ##__VA_ARGS__ )
#else
#	define LOG_WARN( x, ... )
#endif

#ifdef DEBUG_LOG_ERROR
#define LOG_ERROR( x, ... ) LOG(ANSI_FG_COLOR_RED, "ERROR ", x, ##__VA_ARGS__ )
#else
#	define LOG_ERROR( x, ... )
#endif

#ifdef DEBUG_LOG_ASSERT
#define LOG_ASSERT( x, ... ) LOG(ANSI_FG_COLOR_RED, "ASSERT", x, ##__VA_ARGS__ )
#else
#	define LOG_ASSERT( x, ... )
#endif

#ifdef DEBUG_LOG_DEBUG
#define LOG_DEBUG( x, ... ) LOG(ANSI_FG_COLOR_BLUE, "DEBUG ", x, ##__VA_ARGS__ )
#else
#	define LOG_DEBUG( x, ... )
#endif

#endif
