/*
 * can.c
 *
 *  Created on: Apr 29, 2014
 *      Author: dave
 */

#include "can.h"
#include "gpio.h"
#include "timer.h"
#include "debug.h"

typedef void (*timeout_function)(void);
void CAN_dummyTimeoutFunction(){}

static struct
{
  bool can1_initialized;
  uint8_t filter_bank_count_can1;

#if HAS_CAN2
  bool can2_initialized;
  uint8_t filter_bank_count_can2;
#endif


  enum CAN_eBoardId board_id;

  uint8_t timeout_times[2][3];              //timeouts for each mailbox on CAN1 ([0][x]) and CAN2 ([1][x])
  timeout_function timeout_functions[2][3]; //called when corresponding timeout_times reach 0

  CanTxMsg HeartbeatMessage;
  const Timer_MyDef* sCANTimer;
} sCANInfo = { false, 0, CAN_BOARD_UNKNOWN, { { 0, 0, 0 }, { 0, 0, 0 } },
               {{CAN_dummyTimeoutFunction,CAN_dummyTimeoutFunction,CAN_dummyTimeoutFunction},
                {CAN_dummyTimeoutFunction,CAN_dummyTimeoutFunction,CAN_dummyTimeoutFunction}},
                {CAN_MSG_GLOBAL_HEARTBEAT << CAN_PARAM_BOARD_ID_BITS | CAN_BOARD_UNKNOWN ,1, CAN_ID_STD, CAN_RTR_DATA, 0, {1,2,3,4,5,6,7,8}},
               CAN_TIMER };

//--------------------------------------------------------------------------------------------------
//private functions


static void CAN_sendHeartbeatToInitializedCANs()
{
  if (sCANInfo.can1_initialized)
    CAN_Transmit(CAN1, &sCANInfo.HeartbeatMessage);

#if HAS_CAN2
  if (sCANInfo.can2_initialized)
    CAN_Transmit(CAN2, &sCANInfo.HeartbeatMessage);
#endif
}

void CAN_TIMER_IRQHandler( )
{
  static unsigned heartbeat_count = 0;
  if (TIM_GetITStatus(sCANInfo.sCANTimer->timx_, TIM_IT_Update))
  {
    TIM_ClearITPendingBit(sCANInfo.sCANTimer->timx_, TIM_IT_Update);

    heartbeat_count = (heartbeat_count + 1) % (CAN_PARAM_TIMEOUT_FREQUENCY / CAN_PARAM_HEARTBEAT_FREQUENCY);
    if (heartbeat_count == 0)
      CAN_sendHeartbeatToInitializedCANs();

    int i = 0;
    for (; i < 3; i++)
    {
      if (sCANInfo.timeout_times[0][i])
      {
        if(--sCANInfo.timeout_times[0][i] == 0)
        {
          sCANInfo.timeout_functions[0][i]();
        }
      }
      if (sCANInfo.timeout_times[1][i])
      {
        if(--sCANInfo.timeout_times[1][i] == 0)
        {
          sCANInfo.timeout_functions[1][i]();
        }
      }
    }
  }
  else
  {
    assert(false);
  }
}

void CAN1_TX_IRQHandler()
{
  //reset timeout timers
  uint32_t transmit_status = CAN1->TSR;
  if(transmit_status & CAN_TSR_RQCP0)
  {
    CAN1->TSR |= CAN_TSR_RQCP0;
    sCANInfo.timeout_times[0][0] = 0;
  }
  else if(transmit_status & CAN_TSR_RQCP1)
  {
    CAN1->TSR |= CAN_TSR_RQCP1;
    sCANInfo.timeout_times[0][1] = 0;
  }
  else if(transmit_status & CAN_TSR_RQCP2)
  {
    CAN1->TSR |= CAN_TSR_RQCP2;
    sCANInfo.timeout_times[0][2] = 0;
  }
  else
    assert(false);
}

#if HAS_CAN2
void CAN2_TX_IRQHandler()
{
  //reset timeout
  uint32_t transmit_status = CAN2->TSR;
  if(transmit_status & CAN_TSR_RQCP0)
  {
    CAN2->TSR |= CAN_TSR_RQCP0;
    sCANInfo.timeout_times[1][0] = 0;
  }
  else if(transmit_status & CAN_TSR_RQCP1)
  {
    CAN2->TSR |= CAN_TSR_RQCP1;
    sCANInfo.timeout_times[1][1] = 0;
  }
  else if(transmit_status & CAN_TSR_RQCP2)
  {
    CAN2->TSR |= CAN_TSR_RQCP2;
    sCANInfo.timeout_times[1][2] = 0;
  }
  else
    assert(false);
}
#endif

//--------------------------------------------------------------------------------------------------
// public functions

int CAN_addMaskFilters16Bit(CAN_TypeDef* canx, uint16_t id1, uint16_t match_mask1, uint16_t id2, uint16_t match_mask2)
{
  if( sCANInfo.filter_bank_count_can1 >= CAN_PARAM_MAX_FILTER_BANKS_CAN1 )
    return -1;


  CAN_FilterInitTypeDef CAN_FilterInitStructure;

#if HAS_CAN2
  if(  sCANInfo.filter_bank_count_can2 >= CAN_PARAM_MAX_FILTER_BANKS_CAN2 )
    return -1;

  if (canx == CAN1)
#endif

    CAN_FilterInitStructure.CAN_FilterNumber = sCANInfo.filter_bank_count_can1++; // filter banks CAN1
#if HAS_CAN2
  else
    CAN_FilterInitStructure.CAN_FilterNumber = CAN_PARAM_MAX_FILTER_BANKS_CAN1 + sCANInfo.filter_bank_count_can2++; // up to CAN_PARAM_MAX_FILTER_BANKS_CAN2 filter banks may be used for CAN2
#endif

  CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
  CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_16bit;
  CAN_FilterInitStructure.CAN_FilterIdHigh = id1 << 5; //stupid peripheral library is stupid
  CAN_FilterInitStructure.CAN_FilterMaskIdHigh = match_mask1 << 5;
  CAN_FilterInitStructure.CAN_FilterIdLow = id2 << 5;
  CAN_FilterInitStructure.CAN_FilterMaskIdLow = match_mask2 << 5;
  CAN_FilterInitStructure.CAN_FilterFIFOAssignment = canx == CAN1 ? CAN_Filter_FIFO0 : CAN_Filter_FIFO1; //CAN1 -> FIFO 0 ; CAN2 -> FIFO 1
  CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
  CAN_FilterInit(&CAN_FilterInitStructure);

  return 0;
}

int CAN_addIdFilter16Bit(CAN_TypeDef* canx, uint16_t id1, uint16_t id2, uint16_t id3, uint16_t id4)
{
  if (sCANInfo.filter_bank_count_can1 >= CAN_PARAM_MAX_FILTER_BANKS_CAN1 )
  	return -1;

  CAN_FilterInitTypeDef CAN_FilterInitStructure;

#if HAS_CAN2
	if( sCANInfo.filter_bank_count_can2 >= CAN_PARAM_MAX_FILTER_BANKS_CAN2)
    return -1;

  if (canx == CAN1)
#endif
    CAN_FilterInitStructure.CAN_FilterNumber = sCANInfo.filter_bank_count_can1++; // up to 20 filter banks may be used for CAN1

#if HAS_CAN2
  else
    CAN_FilterInitStructure.CAN_FilterNumber = CAN_PARAM_MAX_FILTER_BANKS_CAN1 + sCANInfo.filter_bank_count_can2++; // up to 8 filter banks may be used for CAN2
#endif

  CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdList;
  CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_16bit;
  CAN_FilterInitStructure.CAN_FilterIdHigh = id1 << 5; //stupid peripheral library is stupid
  CAN_FilterInitStructure.CAN_FilterIdLow = id2 << 5;
  CAN_FilterInitStructure.CAN_FilterMaskIdHigh = id3 << 5;
  CAN_FilterInitStructure.CAN_FilterMaskIdLow = id4 << 5;
  CAN_FilterInitStructure.CAN_FilterFIFOAssignment = canx == CAN1 ? CAN_Filter_FIFO0 : CAN_Filter_FIFO1; //CAN1 -> FIFO 0 ; CAN2 -> FIFO 1
  CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
  CAN_FilterInit(&CAN_FilterInitStructure);

  return 0;
}

int CAN_init(enum CAN_eBoardId board_id, const CANRx_MyDef* can_rx, const CANTx_MyDef* can_tx,
             uint8_t irq_priority, uint8_t irq_subpriority, enum CAN_eBaudrate baudrate)
{

  if (can_tx->canx_ != can_rx->canx_)
    return -1;

#ifdef STM32F103XX
  // on the f103 we need to start this clock, don't know
  // if we need to do this on the 405 too
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_AFIO, ENABLE );
#endif

  // //GPIO configuration
  // Connect CAN pins to AF
#ifdef STM32F405XX
  GPIO_PinAFConfig(can_rx->gpio_struct_.gpiox_, can_rx->gpio_pinsource_, can_rx->gpio_af_canx_);
  GPIO_PinAFConfig(can_tx->gpio_struct_.gpiox_, can_tx->gpio_pinsource_, can_tx->gpio_af_canx_);
#else
  GPIO_PinRemapConfig( can_rx->remap_, ENABLE );
#endif

  // Configure CAN RX and TX pins
  GPIO_MyInitDef GPIO_InitStructure;
  GPIO_InitStructure.pin_mode 	 = GPIO_PIN_AF_INPUT;
  GPIO_InitStructure.output_mode = GPIO_OUTPUT_PP;
  GPIO_InitStructure.pull_mode   = GPIO_PULL_UP;
  GPIO_InitStructure.pin_speed   = GPIO_SPEED_HIGH;
  GPIO_init( can_rx->gpio_struct_, &GPIO_InitStructure );

  GPIO_InitStructure.pin_mode = GPIO_PIN_AF;
  GPIO_init( can_tx->gpio_struct_, &GPIO_InitStructure );

  // // CAN configuration
  // Enable CAN clock
#ifdef HAS_CAN2
  if (can_rx->canx_ == CAN2) // CAN2 is slave, needs CAN1 clock enabled
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);
#endif

  RCC_APB1PeriphClockCmd(can_rx->rcc_apb1periph_canx_, ENABLE); //==can_tx->rcc...

  // CAN register deinit
  CAN_DeInit(can_rx->canx_);

  // CAN cell init
  CAN_InitTypeDef CAN_InitStructure;
  CAN_InitStructure.CAN_ABOM = ENABLE;  // auto recover from bus-off error
  CAN_InitStructure.CAN_AWUM = DISABLE;
  CAN_InitStructure.CAN_NART = DISABLE; // enable automatic retransmission
  CAN_InitStructure.CAN_TTCM = DISABLE; // no TTCAN implementation
  CAN_InitStructure.CAN_RFLM = DISABLE;
  CAN_InitStructure.CAN_TXFP = ENABLE;  // transmit higher priority messages first
  CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;

  // Important: Baudrate = CAN_CLK / (CAN_Prescaler * (CAN_SJW + CAN_BS1 + CAN_BS2))!!!!
#ifdef STM32F405XX
  // (15+1)/(15+5+1) = Sampling point at about 75% (76,19%) allows for more oscillator tolerance
  CAN_InitStructure.CAN_SJW = CAN_SJW_1tq;
  CAN_InitStructure.CAN_BS1 = CAN_BS1_15tq;
  CAN_InitStructure.CAN_BS2 = CAN_BS2_5tq;
#elif defined(STM32F103XX)
  // (1+8)/(1+8+3) = 75.00%
  CAN_InitStructure.CAN_SJW = CAN_SJW_1tq;
  CAN_InitStructure.CAN_BS1 = CAN_BS1_8tq;
  CAN_InitStructure.CAN_BS2 = CAN_BS2_3tq;
#else
#	error "No can settings found!";
#endif

  CAN_InitStructure.CAN_Prescaler = baudrate;
  CAN_Init(can_rx->canx_, &CAN_InitStructure);

  sCANInfo.board_id = board_id;

  //initialize heartbeat message
  sCANInfo.HeartbeatMessage.StdId = (CAN_MSG_GLOBAL_HEARTBEAT << CAN_PARAM_BOARD_ID_BITS) | board_id;
  sCANInfo.HeartbeatMessage.ExtId = 0x01;
  sCANInfo.HeartbeatMessage.RTR = CAN_RTR_DATA;
  sCANInfo.HeartbeatMessage.IDE = CAN_ID_STD;
  sCANInfo.HeartbeatMessage.DLC = 0;


  // Interrupt initialization
  // RX Interrupt
  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_InitStructure.NVIC_IRQChannel = can_rx->nvci_irq_;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = irq_priority;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = irq_subpriority;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  // TX Interrupt
  NVIC_InitStructure.NVIC_IRQChannel = can_rx->nvci_irq_;
  NVIC_Init(&NVIC_InitStructure);

  if (can_rx->canx_ == CAN1){
    sCANInfo.can1_initialized = true;
    CAN_ITConfig(can_rx->canx_, CAN_IT_FMP0, ENABLE); //FIFO 0 Message pending interrupt
  }
#if HAS_CAN2
  else{
    sCANInfo.can2_initialized = true;
    CAN_ITConfig(can_rx->canx_, CAN_IT_FMP1, ENABLE); //FIFO 1 Message pending interrupt
  }
#endif
  CAN_ITConfig(can_rx->canx_, CAN_IT_TME, ENABLE); //Transmit interrupt

  //initialize CAN timeout/heartbeat timer
  TIMER_initPeriodicInterrupt( sCANInfo.sCANTimer, CAN_PARAM_TIMEOUT_FREQUENCY, CAN_PARAM_TIMEOUT_IRQ_PRIORITY , CAN_PARAM_TIMEOUT_IRQ_SUBPRIORITY);

  return 0;
}

int CAN_writeData(CAN_TypeDef* canx, enum CAN_eMessageId msg_id, uint8_t* data, uint8_t length_in_bytes)
{
  CanTxMsg TxMessage;
  TxMessage.StdId = (msg_id << CAN_PARAM_BOARD_ID_BITS) | sCANInfo.board_id;
  TxMessage.ExtId = 0x01;
  TxMessage.RTR = CAN_RTR_DATA;
  TxMessage.IDE = CAN_ID_STD;
  TxMessage.DLC = length_in_bytes;

  int i = 0;
  for (; i < length_in_bytes; i++)
    TxMessage.Data[i] = *(data + i);

  uint8_t transmit_mailbox = CAN_Transmit(canx, &TxMessage);

  if (transmit_mailbox == CAN_TxStatus_NoMailBox)
    return -1;

  return 0;
}

int CAN_writeDataTimeout(CAN_TypeDef* canx, enum CAN_eMessageId msg_id, uint8_t* data, uint8_t length_in_bytes, uint8_t timeout, void (*timeout_function)())
{
  CanTxMsg TxMessage;
  TxMessage.StdId = (msg_id << CAN_PARAM_BOARD_ID_BITS) | sCANInfo.board_id;
  TxMessage.ExtId = 0x01;
  TxMessage.RTR = CAN_RTR_DATA;
  TxMessage.IDE = CAN_ID_STD;
  TxMessage.DLC = length_in_bytes;

  int i = 0;
  for (; i < length_in_bytes; i++)
    TxMessage.Data[i] = *(data + i);

  uint8_t transmit_mailbox = CAN_Transmit(canx, &TxMessage);

  if (transmit_mailbox == CAN_TxStatus_NoMailBox)
    return -1;

  int storage = canx == CAN1 ? 0 : 1;
  sCANInfo.timeout_times[storage][transmit_mailbox] = timeout+1; //Timeout can get decremented immediately after setting it (before intended timeout), therefore add 1 time quantum
  sCANInfo.timeout_functions[storage][transmit_mailbox] = timeout_function;

  return 0;
}



int CAN_writeRemoteTransmissionRequest(CAN_TypeDef* canx, enum CAN_eMessageId msg_id)
{
  CanTxMsg TxMessage;
  TxMessage.StdId = (msg_id << CAN_PARAM_BOARD_ID_BITS) | sCANInfo.board_id;
  TxMessage.ExtId = 0x01;
  TxMessage.RTR = CAN_RTR_REMOTE;
  TxMessage.IDE = CAN_ID_STD;
  TxMessage.DLC = 0;

  uint8_t transmit_mailbox = CAN_Transmit(canx, &TxMessage);

  if (transmit_mailbox == CAN_TxStatus_NoMailBox)
    return -1;

  return 0;
}

int CAN_writeRemoteTransmissionRequestTimeout(CAN_TypeDef* canx, enum CAN_eMessageId msg_id, uint8_t timeout, void (*timeout_function)())
{
  CanTxMsg TxMessage;
  TxMessage.StdId = (msg_id << CAN_PARAM_BOARD_ID_BITS) | sCANInfo.board_id;
  TxMessage.ExtId = 0x01;
  TxMessage.RTR = CAN_RTR_REMOTE;
  TxMessage.IDE = CAN_ID_STD;
  TxMessage.DLC = 0;

  uint8_t transmit_mailbox = CAN_Transmit(canx, &TxMessage);

  if (transmit_mailbox == CAN_TxStatus_NoMailBox)
    return -1;

  int storage = canx == CAN1 ? 0 : 1;
  sCANInfo.timeout_times[storage][transmit_mailbox] = timeout+1; //Timeout can get decremented immediately after setting it (before intended timeout), therefore add 1 time quantum
  sCANInfo.timeout_functions[storage][transmit_mailbox] = timeout_function;

  return 0;
}

int CAN_read(CAN_TypeDef* canx, enum CAN_eMessageId* msg_id, enum CAN_eBoardId* board_id,  uint8_t* data, uint8_t* data_length_in_bytes)
{
  CanRxMsg rxMsg;

  if(canx == CAN1)
    CAN_Receive(canx, CAN_FIFO0, &rxMsg);
  else
    CAN_Receive(canx, CAN_FIFO1, &rxMsg);

  *board_id = rxMsg.StdId & CAN_PARAM_BOARD_ID_MASK;
  *msg_id = (rxMsg.StdId & (0x7FF & ~CAN_PARAM_BOARD_ID_MASK)) >> CAN_PARAM_BOARD_ID_BITS;

  *data_length_in_bytes = rxMsg.DLC;
  int i = 0;
  for(; i < rxMsg.DLC; i++)
    data[i] = rxMsg.Data[i];

  return 0;
}

