/*
 * adc_dma.c
 *
 *  Created on: Mar 6, 2014
 *      Author: dave
 */

#include "adc_dma.h"

#define MAX_ADC_CHANNEL 18
#define ADC_STORAGE_INIT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0} //MAX_ADC_CHANNEL * 0

#define BUFFERSIZE 32 //== number of samples to average

__attribute__ ((section (".sram"))) uint16_t ADC1_temp_storage[MAX_ADC_CHANNEL] = ADC_STORAGE_INIT;
__attribute__ ((section (".sram"))) uint16_t ADC2_temp_storage[MAX_ADC_CHANNEL] = ADC_STORAGE_INIT;

uint16_t ADC1_avg_storage[MAX_ADC_CHANNEL] = ADC_STORAGE_INIT;
uint16_t ADC2_avg_storage[MAX_ADC_CHANNEL] = ADC_STORAGE_INIT;

#ifdef HAS_ADC3
__attribute__ ((section (".sram"))) uint16_t ADC3_temp_storage[MAX_ADC_CHANNEL] = ADC_STORAGE_INIT;
uint16_t ADC3_avg_storage[MAX_ADC_CHANNEL] = ADC_STORAGE_INIT;
#endif

//------------------------------------------------------------------------------
// DMA interrupts
//
void DMA2_Stream0_IRQHandler()
{
  if (DMA_GetITStatus(DMA2_Stream0, DMA_IT_TCIF0)) //NOTE: make modular...
  {
    DMA_ClearITPendingBit(DMA2_Stream0, DMA_IT_TCIF0);
    static uint32_t ADC1_sum_storage[MAX_ADC_CHANNEL] = ADC_STORAGE_INIT;

    unsigned i = 0;
    for (i = 0; i < MAX_ADC_CHANNEL; i++)
    {
      ADC1_sum_storage[i] += ADC1_temp_storage[i];
    }

    static uint32_t buffercnt = 0;
    buffercnt = (buffercnt + 1) % BUFFERSIZE;
    if (buffercnt == 0)
    {
      for (i = 0; i < MAX_ADC_CHANNEL; i++)
      {
        ADC1_avg_storage[i] = ADC1_sum_storage[i] / BUFFERSIZE;
        ADC1_sum_storage[i] = 0;
      }
    }
  }
}

void DMA2_Stream1_IRQHandler()
{
  if (DMA_GetITStatus(DMA2_Stream1, DMA_IT_TCIF1)) //NOTE: make modular...
  {
    DMA_ClearITPendingBit(DMA2_Stream1, DMA_IT_TCIF1);
    static uint32_t ADC3_sum_storage[MAX_ADC_CHANNEL] = ADC_STORAGE_INIT;

    unsigned i = 0;
    for (i = 0; i < MAX_ADC_CHANNEL; i++)
    {
      ADC3_sum_storage[i] += ADC1_temp_storage[i];
    }

    static uint32_t buffercnt = 0;
    buffercnt = (buffercnt + 1) % BUFFERSIZE;
    if (buffercnt == 0)
    {
      for (i = 0; i < MAX_ADC_CHANNEL; i++)
      {
        ADC3_avg_storage[i] = ADC3_sum_storage[i] / BUFFERSIZE;
        ADC3_sum_storage[i] = 0;
      }
    }
  }
}

void DMA2_Stream3_IRQ_Handler()
{
  if (DMA_GetITStatus(DMA2_Stream3, DMA_IT_TCIF3)) //NOTE: make modular...
  {
    DMA_ClearITPendingBit(DMA2_Stream3, DMA_IT_TCIF3);
    static uint32_t ADC2_sum_storage[MAX_ADC_CHANNEL] = ADC_STORAGE_INIT;

    unsigned i = 0;
    for (i = 0; i < MAX_ADC_CHANNEL; i++)
    {
      ADC2_sum_storage[i] += ADC1_temp_storage[i];
    }

    static uint32_t buffercnt = 0;
    buffercnt = (buffercnt + 1) % BUFFERSIZE;
    if (buffercnt == 0)
    {
      for (i = 0; i < MAX_ADC_CHANNEL; i++)
      {
        ADC2_avg_storage[i] = ADC2_sum_storage[i] / BUFFERSIZE;
        ADC2_sum_storage[i] = 0;
      }
    }
  }
}

//------------------------------------------------------------------------------
// Increments, then returns the number of used channels for the given ADC
// @param ADCx
// @return the increased value
uint32_t incInitializedChannelCount(ADC_TypeDef* ADCx)
{
  static uint32_t adc1_channel_cnt = 0;
  static uint32_t adc2_channel_cnt = 0;
  static uint32_t adc3_channel_cnt = 0;

  uint32_t rank = 0;
  if (ADCx == ADC1)
  {
    rank = ++adc1_channel_cnt;
  }
  else if (ADCx == ADC2)
  {
    rank = ++adc2_channel_cnt;
  }
  else if (ADCx == ADC3)
  {
    rank = ++adc3_channel_cnt;
  }
  else
  {
    while (1)
      ; //configure when needed
  }

  return rank;
}

uint16_t* lookupTempDMAMemoryStorage(ADC_TypeDef* ADCx)
{
  uint16_t* storage = 0;
  if (ADCx == ADC1)
  {
    storage = ADC1_temp_storage;
  }
  else if (ADCx == ADC2)
  {
    storage = ADC2_temp_storage;
  }
  else if (ADCx == ADC3)
  {
    storage = ADC3_temp_storage;
  }
  else
  {
    while (1)
      ; //configure when needed
  }

  return storage;
}

uint16_t* lookupAvgDMAMemoryStorage(ADC_TypeDef* ADCx)
{
  uint16_t* storage = 0;
  if (ADCx == ADC1)
  {
    storage = ADC1_avg_storage;
  }
  else if (ADCx == ADC2)
  {
    storage = ADC2_avg_storage;
  }
  else if (ADCx == ADC3)
  {
    storage = ADC3_avg_storage;
  }
  else
  {
    while (1)
      ; //configure when needed
  }

  return storage;
}

uint16_t* ADC_initWithDMA(const ADC_MyDef* adcx_channely, const DMA_MyDef* dmax_streamy, uint32_t dma_channelx)
{
  ADC_InitTypeDef ADC_InitStructure;
  ADC_CommonInitTypeDef ADC_CommonInitStructure;
  DMA_InitTypeDef DMA_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;

  /* Enable ADCx, DMA and GPIO clocks ****************************************/
  RCC_AHB1PeriphClockCmd(dmax_streamy->rcc_ahb1periph_dmax_, ENABLE);
  RCC_AHB1PeriphClockCmd(adcx_channely->gpio_struct_.rcc_ahb1periph_gpiox_, ENABLE);
  RCC_APB2PeriphClockCmd(adcx_channely->rcc_apb2periph_adcx_, ENABLE);

  uint32_t rank = incInitializedChannelCount(adcx_channely->adcx_);
  uint16_t* temp_storage_addr = lookupTempDMAMemoryStorage(adcx_channely->adcx_);

  /* DMA2 Stream channel configuration **************************************/
  DMA_ITConfig(dmax_streamy->dmax_streamy_, DMA_IT_TC, DISABLE);
  DMA_DeInit(dmax_streamy->dmax_streamy_);

  DMA_InitStructure.DMA_Channel = dma_channelx;
  DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &(adcx_channely->adcx_->DR);
  DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) temp_storage_addr;

  DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
  DMA_InitStructure.DMA_BufferSize = rank; //storage_places_cnt;

  DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;

  DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord; // ADC -> HalfWords
  DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;

  DMA_InitStructure.DMA_Mode = DMA_Mode_Circular; //no reinit after 1 transfer necessary
  DMA_InitStructure.DMA_Priority = DMA_Priority_High;

  DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
  DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull; //Will be ignored in direct mode
  DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single; //no burst transfers allowed in direct mode
  DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;

  DMA_Init(dmax_streamy->dmax_streamy_, &DMA_InitStructure);
  DMA_Cmd(dmax_streamy->dmax_streamy_, ENABLE);

  /* Configure ADC Channel pin as analog input ******************************/
  GPIO_InitStructure.GPIO_Pin =  adcx_channely->gpio_struct_.gpio_pin_x_;//gpio_init.GPIO_Pinx_;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(adcx_channely->gpio_struct_.gpiox_, &GPIO_InitStructure);

  /* ADC Common Init **********************************************************/
  ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
  ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div8;
  ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled; //only relevant in multi ADC mode
  ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_20Cycles;
  ADC_CommonInit(&ADC_CommonInitStructure);

  /* ADC Init ****************************************************************/
  ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
  ADC_InitStructure.ADC_ScanConvMode = ENABLE;
  ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
  ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
  ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1; //ignored (ADC_ExternalTrigConvEdge_None)
  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  ADC_InitStructure.ADC_NbrOfConversion = rank; //storage_places_cnt;
  ADC_Init(adcx_channely->adcx_, &ADC_InitStructure);

  /* ADC regular channel configuration *************************************/
  ADC_RegularChannelConfig(adcx_channely->adcx_, adcx_channely->adc_channel_x_, rank,
  ADC_SampleTime_480Cycles);

  /* Enable DMA request after last transfer (Single-ADC mode) */
  ADC_DMARequestAfterLastTransferCmd(adcx_channely->adcx_, ENABLE);

  /* Enable ADC DMA */
  ADC_DMACmd(adcx_channely->adcx_, ENABLE);

  /* Enable ADC */
  ADC_Cmd(adcx_channely->adcx_, ENABLE);

  /* Enable DMA Stream Transfer Complete interrupt */
  DMA_ITConfig(dmax_streamy->dmax_streamy_, DMA_IT_TC, ENABLE);

  /* Enable the DMA Stream IRQ Channel */
  NVIC_InitTypeDef NVIC_InitStructure;
  NVIC_InitStructure.NVIC_IRQChannel = dmax_streamy->dmax_streamy_irq_;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  uint16_t* avg_storage_addr = lookupAvgDMAMemoryStorage(adcx_channely->adcx_);
  return &avg_storage_addr[rank - 1];
}
