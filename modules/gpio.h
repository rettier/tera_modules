/*
 * can.h
 *
 *  Created on: Apr 28, 2014
 *      Author: dave
 */

#ifndef __MODULES__MODULES_STM32FX__GPIO_H__
#define __MODULES__MODULES_STM32FX__GPIO_H__

#include "stm32fx_modules.h"

/**
  * @brief  GPIO output mode configuration enumeration
  */
typedef enum
{
  GPIO_OUTPUT_PP = 0x00, /*!< push-pull output mode */
  GPIO_OUTPUT_OD = 0x01  /*!< open drain output mode */
} GPIO_eOutputMode;

/**
  * @brief  GPIO input mode configuration enumeration
  */
typedef enum
{
  GPIO_PULL_NONE = 0x00, /*!< floating input, no pull resistor */
  GPIO_PULL_UP   = 0x01, /*!< pull up input mode */
  GPIO_PULL_DOWN = 0x02  /*!< pull down input mode */
} GPIO_ePullMode;

/**
  * @brief  GPIO pin mode configuration enumeration
  */
typedef enum
{
  GPIO_PIN_IN  	  	= 0x00, /*!< Digital input */
  GPIO_PIN_OUT 	  	= 0x01, /*!< Digital output */
  GPIO_PIN_AF 		  = 0x02, /*!< Alternate function */
  GPIO_PIN_AIN 		  = 0x03, /*!< Analog input */

  GPIO_PIN_AF_INPUT = GPIO_AF_INPUT_MODE_, /*!< Compatibility mode, AF on F405, INPUT on F103 */

} GPIO_ePinMode;

/**
  * @brief  GPIO pin speed configuration enumeration
  */
typedef enum
{
  GPIO_SPEED_LOW    = 0x00, /*!< 2MHz */
  GPIO_SPEED_MEDIUM = 0x01, /*!< 10Mhz on F103, 25Mhz on F405 */
  GPIO_SPEED_FAST   = 0x02, /*!< 50Mhz */
  GPIO_SPEED_HIGH   = 0x03  /*!< 100Mhz only on F405, ..._HIGH on F103 */
} GPIO_ePinSpeed;

typedef struct _GPIO_MyInitDef_
{
  GPIO_ePullMode   pull_mode;
  GPIO_eOutputMode output_mode;
  GPIO_ePinMode    pin_mode;
  GPIO_ePinSpeed   pin_speed;
} GPIO_MyInitDef;

extern void GPIO_structInit( GPIO_MyInitDef* init );
extern void GPIO_init( const GPIO_MyDef * GPIOX, GPIO_MyInitDef* init );

extern uint8_t GPIO_readInputDataBit( const GPIO_MyDef *pin );
extern uint8_t GPIO_readOutputDataBit( const GPIO_MyDef *pin );
extern void GPIO_writeBit( const GPIO_MyDef *pin, BitAction BitVal );
extern void GPIO_toggleBit( const GPIO_MyDef *pin );

#endif // __MODULES__MODULES_STM32FX__GPIO_H__
