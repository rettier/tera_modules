/*
 * can_defines_ibex.h
 *
 *  Created on: Apr 29, 2014
 *      Author: dave
 */

#ifndef __COMMON__CAN_DEFINES_IBEX_H__
#define __COMMON__CAN_DEFINES_IBEX_H__

#ifdef STM32F40XX //select uC in your <project>_config.h file
  #define MAX_FILTER_BANKS 28
#elif defined(STM32F103XX)
  #define MAX_FILTER_BANKS 14
#else
  #error "Select a microcontroller in your <project>_config.h file"
#endif

// Following parameters may be adjusted
enum
{
  CAN_PARAM_MESSAGE_ID_BITS = 6,
  CAN_PARAM_BOARD_ID_MASK = 0x1F,
  CAN_PARAM_BOARD_ID_BITS = (11 - CAN_PARAM_MESSAGE_ID_BITS),
  CAN_PARAM_MAX_FILTER_BANKS_CAN1 = 14,                                     //filter banks 0 to CAN_PARAM_MAX_FILTER_BANKS_CAN1 - 1 are used for CAN1
  CAN_PARAM_MAX_FILTER_BANKS_CAN2 = (MAX_FILTER_BANKS - CAN_PARAM_MAX_FILTER_BANKS_CAN1), //filter banks CAN_PARAM_MAX_FILTER_BANKS_CAN1 till maximum used for CAN2
  CAN_PARAM_HEARTBEAT_FREQUENCY = 10, //in Hz
  CAN_PARAM_TIMEOUT_FREQUENCY = 2000, //in Hz
  CAN_PARAM_TIMEOUT_IRQ_PRIORITY = 0xF,
  CAN_PARAM_TIMEOUT_IRQ_SUBPRIORITY = 0xF
};


enum CAN_eBoardId //5bit
{
  CAN_BOARD_TOTAL_CONTROLLER = 0x00, //highest priority board

  CAN_BOARD_PEDALS1 = 0x20,

  CAN_BOARD_LIGHT_CONTROLLER  = 0x10,
  CAN_BOARD_LIGHT_BACK_LEFT   = 0x11,
  CAN_BOARD_LIGHT_BACK_RIGHT  = 0x12,
  CAN_BOARD_LIGHT_FRONT_LEFT  = 0x13,
  CAN_BOARD_LIGHT_FRONT_RIGHT = 0x14,

  CAN_BOARD_TEST_1 = 0x1D,
  CAN_BOARD_TEST_2 = 0x1E,
  CAN_BOARD_UNKNOWN = 0x1F // lowest priority board
};

enum CAN_eMessageId //6bit
{
  // global message ids are used by multiple boards
  CAN_MSG_GLOBAL_HEARTBEAT = 0x3F, //0x3F == least priority
  CAN_MSG_GLOBAL_TEST = 0x3E,

  // board specific messages

  // pedals
  CAN_MSG_PEDAL_DATA = 0x11,
  CAN_MSG_PEDAL_ERROR = 0x10,

  // temperature control
  TEMPERATURE_CONTROL_MSG1 = 0x0, TEMPERATURE_CONTROL_MSG2 = 0x3F,

  //fan control
  FAN_CONTROL_MSG1 = 0x0, FAN_CONTROL_MSG2 = 0x3F
};

#endif // __COMMON__CAN_DEFINES_IBEX_H__
