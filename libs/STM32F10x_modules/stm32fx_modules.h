#ifndef __MODULES__STM32FX_MODULES_H__
#define __MODULES__STM32FX_MODULES_H__

#include "stm32f10x.h"
#include "stm32f10x_conf.h"

#include "modules_def.h"
#include "modules_structs.h"
#include "modules_board_def.h"
#include "modules_carrier_def.h"

#endif //__MODULES__STM32FX_MODULES_H__
