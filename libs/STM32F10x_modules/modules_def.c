/*
 * stm32f4_def.c
 *
 *  Created on: Apr 15, 2014
 *      Author: dave
 */

#include "stm32fx_modules.h"

#define INIT_ADC_INFO(number) \
		__attribute__ ((section (".sram"))) uint16_t ADC##number##_temp_storage_[ADC_MAX_CHANNELS]; \
		ADC_Info_MyDef ADC##number##_Info_ = { .avg_hook_ = 0, .temp_storage_ = ADC##number##_temp_storage_ };

#if ADC_MODULE_COUNT >= 1
	INIT_ADC_INFO(1);
#endif

#if ADC_MODULE_COUNT >= 2
	INIT_ADC_INFO(2);
#endif

#if ADC_MODULE_COUNT >= 3
	INIT_ADC_INFO(3);
#endif
