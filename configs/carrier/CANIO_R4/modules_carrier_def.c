#include "modules_carrier_def.h"

INIT(Led_MyDef, LED1);
INIT(Led_MyDef, LED2);

const Led_MyDef * const LED_leds_[LED_COUNT] = { &LED1_, &LED2_ };
