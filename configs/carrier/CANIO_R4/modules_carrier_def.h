#ifndef __MODULES_CARRIER_DEF_H__
#define __MODULES_CARRIER_DEF_H__

#include "stm32fx_modules.h"

#define LED_COUNT 2

#define LED1_INIT {&PA9_, 1}
#define LED2_INIT {&PA8_, 1}

EXT(Led_MyDef, LED1);
EXT(Led_MyDef, LED2);

#endif // __MODULES_CARRIER_DEF_H__
