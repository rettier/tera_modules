#include "modules_board_def.h"
#include "tle8104e.h"
#include "its5215l.h"
#include "debug.h"

TLE8104EInfo_MyDef TLE8104E0_info = { 0, 0 };
INIT(TLE8104E_MyDef, TLE8104E0);

void EXTI3_IRQHandler(void)
{
  if(EXTI_GetITStatus(EXTI_Line3) != RESET)
  {
  	TLE8104E_irq_( TLE8104E0 );
    EXTI_ClearITPendingBit(EXTI_Line3);
  }
}

ITS5215L_Info ITS5215L0_Info_ = { { ITS5215_NO_ERROR, ITS5215_NO_ERROR } };
INIT(ITS5215L_MyDef, ITS5215L0);

void EXTI0_IRQHandler(void)
{
	if(EXTI_GetITStatus(EXTI_Line0) != RESET)
	{
		ITS5215L_irq_( ITS5215L0, 1 );
		EXTI_ClearITPendingBit(EXTI_Line0);
	}
}

void EXTI1_IRQHandler(void)
{
	if(EXTI_GetITStatus(EXTI_Line1) != RESET)
	{
		ITS5215L_irq_( ITS5215L0, 0 );
		EXTI_ClearITPendingBit(EXTI_Line1);
	}
}
