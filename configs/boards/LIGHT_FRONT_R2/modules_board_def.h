#ifndef __MODULES_BOARD_FRONT_DEF_H__
#define __MODULES_BOARD_FRONT_DEF_H__

#include "stm32fx_modules.h"

#ifdef SIDE_FRONT
#define LIGHT_UPPER_CHANNEL    0
#define LIGHT_LOWER_CHANNEL    1

#define LIGHT_POSITION_CHANNEL 0
#define LIGHT_PLATE_CHANNEL    1
#define LIGHT_SIGNAL_CHANNEL   2
#endif

#ifdef SIDE_BACK
#define LIGHT_BREAK_CHANNEL    0
#define LIGHT_POSITION_CHANNEL    1

#define LIGHT_FOG_BACK_CHANNEL 0
#define LIGHT_PLATE_CHANNEL    1
#define LIGHT_SIGNAL_CHANNEL   2
#endif


#define TLE8104E0_INIT { &PB10_, &PB14_, &PB15_, &PB13_, &PB11_, &PB12_, \
												 GPIO_PinSource3, GPIO_PortSourceGPIOA, EXTI_Line3, EXTI3_IRQn, \
												 SPI2, &TLE8104E0_info, \
												 RCC_APB1PeriphClockCmd, RCC_APB1Periph_SPI2 }

struct _TLE8104E_MyDef_;
EXT(struct _TLE8104E_MyDef_, TLE8104E0);

#define ITS5215L0_INIT { { &PB0_, &PB1_ }, { &PA2_, &PA3_ }, \
											 	 { GPIO_PinSource0, GPIO_PinSource1 }, \
												 { GPIO_PortSourceGPIOB, GPIO_PortSourceGPIOB }, \
												 { EXTI_Line0, EXTI_Line1 }, \
												 { EXTI0_IRQn, EXTI1_IRQn }, &ITS5215L0_Info_ }

struct _ITS5215L_MyDef_;
EXT(struct _ITS5215L_MyDef_, ITS5215L0);

#endif // __MODULES_BOARD_DEF_H__
