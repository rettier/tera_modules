#include "can_message_handler.h"
#include "can_defines_light.h"

#include "its5215l.h"
#include "tle8104e.h"

#include "delay.h"

extern volatile uint8_t blinker_on;

// this isn't pretty, but using the front board for the back lights isn't either
#ifdef SIDE_FRONT
bool handleMessage( uint8_t msg_id, uint8_t board_id, uint8_t* data, uint8_t data_length )
{
	// ------------------------------------------------------------------------------------
	// Head Light
	// ------------------------------------------------------------------------------------
	switch( msg_id )
	{
	case CAN_MSG_LIGHT_UPPER_ON:
		ITS5215L_on( ITS5215L0, LIGHT_LOWER_CHANNEL );
		ITS5215L_on( ITS5215L0, LIGHT_UPPER_CHANNEL );
		break;
	case CAN_MSG_LIGHT_UPPER_OFF:
		ITS5215L_off( ITS5215L0, LIGHT_UPPER_CHANNEL );
		break;

	case CAN_MSG_LIGHT_LOWER_ON:
		ITS5215L_on( ITS5215L0, LIGHT_LOWER_CHANNEL );
		break;
	case CAN_MSG_LIGHT_LOWER_OFF:
		ITS5215L_off( ITS5215L0, LIGHT_UPPER_CHANNEL );
		ITS5215L_off( ITS5215L0, LIGHT_LOWER_CHANNEL );
		break;

// ------------------------------------------------------------------------------------
// Signal Light
// ------------------------------------------------------------------------------------
#ifdef SIDE_LEFT
	case CAN_MSG_LIGHT_SIGNAL_LEFT_ON:
		blinker_on = 1;
		break;

	case CAN_MSG_LIGHT_SIGNAL_RIGHT_ON:
	case CAN_MSG_LIGHT_SIGNAL_LEFT_OFF:
		blinker_on = 0;
		break;
#endif

#ifdef SIDE_RIGHT
		case CAN_MSG_LIGHT_SIGNAL_RIGHT_ON:
			blinker_on = 1;
		break;

	case CAN_MSG_LIGHT_SIGNAL_LEFT_ON:
	case CAN_MSG_LIGHT_SIGNAL_RIGHT_OFF:
		blinker_on = 0;
		break;
#endif

	case CAN_MSG_LIGHT_SIGNAL_ALL_ON:
	case CAN_MSG_LIGHT_SIGNAL_FRONT_ON:
		blinker_on = 1;
		break;

	case CAN_MSG_LIGHT_SIGNAL_ALL_OFF:
	case CAN_MSG_LIGHT_SIGNAL_FRONT_OFF:
		blinker_on = 0;
		break;


// ------------------------------------------------------------------------------------
// Position Light
// ------------------------------------------------------------------------------------
	case CAN_MSG_LIGHT_POSITION_ALL_ON:
	case CAN_MSG_LIGHT_POSITION_FRONT_ON:
		TLE8104E_setChannelOR( TLE8104E0, LIGHT_POSITION_CHANNEL, 1 );
		break;

	case CAN_MSG_LIGHT_POSITION_ALL_OFF:
	case CAN_MSG_LIGHT_POSITION_FRONT_OFF:
		TLE8104E_setChannelOR( TLE8104E0, LIGHT_POSITION_CHANNEL, 0 );
		break;

#ifdef SIDE_LEFT
	case CAN_MSG_LIGHT_POSITION_LEFT_ON:
		TLE8104E_setChannelOR( TLE8104E0, LIGHT_POSITION_CHANNEL, 1 );
		break;

	case CAN_MSG_LIGHT_POSITION_LEFT_OFF:
		TLE8104E_setChannelOR( TLE8104E0, LIGHT_POSITION_CHANNEL, 0 );
		break;
#endif

#ifdef SIDE_RIGHT
	case CAN_MSG_LIGHT_POSITION_RIGHT_ON:
		TLE8104E_setChannelOR( TLE8104E0, LIGHT_POSITION_CHANNEL, 1 );
		break;

	case CAN_MSG_LIGHT_POSITION_RIGHT_OFF:
		TLE8104E_setChannelOR( TLE8104E0, LIGHT_POSITION_CHANNEL, 0 );
		break;
#endif

// ------------------------------------------------------------------------------------
// Plate Light
// ------------------------------------------------------------------------------------
	case CAN_MSG_LIGHT_PLATE_ALL_ON:
	case CAN_MSG_LIGHT_PLATE_FRONT_ON:
		TLE8104E_setChannelOR( TLE8104E0, LIGHT_PLATE_CHANNEL, 1 );
		break;

	case CAN_MSG_LIGHT_PLATE_ALL_OFF:
	case CAN_MSG_LIGHT_PLATE_FRONT_OFF:
		TLE8104E_setChannelOR( TLE8104E0, LIGHT_PLATE_CHANNEL, 0 );
		break;


	default:
		return false;
	}

	return true;
}
#endif

#ifdef SIDE_BACK
bool handleMessage( uint8_t msg_id, uint8_t board_id, uint8_t* data, uint8_t data_length )
{
	switch( msg_id )
	{
// ------------------------------------------------------------------------------------
// Signal Light
// ------------------------------------------------------------------------------------
#ifdef SIDE_LEFT
	case CAN_MSG_LIGHT_SIGNAL_LEFT_ON:
		blinker_on = 1;
		break;

	case CAN_MSG_LIGHT_SIGNAL_RIGHT_ON:
	case CAN_MSG_LIGHT_SIGNAL_LEFT_OFF:
		blinker_on = 0;
		break;
#endif

#ifdef SIDE_RIGHT
		case CAN_MSG_LIGHT_SIGNAL_RIGHT_ON:
			blinker_on = 1;
		break;

	case CAN_MSG_LIGHT_SIGNAL_LEFT_ON:
	case CAN_MSG_LIGHT_SIGNAL_RIGHT_OFF:
		blinker_on = 0;
		break;
#endif

	case CAN_MSG_LIGHT_SIGNAL_ALL_ON:
	case CAN_MSG_LIGHT_SIGNAL_BACK_ON:
		blinker_on = 1;
		break;

	case CAN_MSG_LIGHT_SIGNAL_ALL_OFF:
	case CAN_MSG_LIGHT_SIGNAL_BACK_OFF:
		blinker_on = 0;
		break;


// ------------------------------------------------------------------------------------
// Position Light
// ------------------------------------------------------------------------------------
	case CAN_MSG_LIGHT_POSITION_ALL_ON:
	case CAN_MSG_LIGHT_POSITION_BACK_ON:
		ITS5215L_on( ITS5215L0, LIGHT_POSITION_CHANNEL );
		break;

	case CAN_MSG_LIGHT_POSITION_ALL_OFF:
	case CAN_MSG_LIGHT_POSITION_BACK_OFF:
		ITS5215L_off( ITS5215L0, LIGHT_POSITION_CHANNEL );
		break;

#ifdef SIDE_LEFT
	case CAN_MSG_LIGHT_POSITION_LEFT_ON:
		ITS5215L_on( ITS5215L0, LIGHT_POSITION_CHANNEL );
		break;

	case CAN_MSG_LIGHT_POSITION_LEFT_OFF:
		ITS5215L_off( ITS5215L0, LIGHT_POSITION_CHANNEL );
		break;
#endif

#ifdef SIDE_RIGHT
	case CAN_MSG_LIGHT_POSITION_RIGHT_ON:
		ITS5215L_on( ITS5215L0, LIGHT_POSITION_CHANNEL );
		break;

	case CAN_MSG_LIGHT_POSITION_RIGHT_OFF:
		ITS5215L_off( ITS5215L0, LIGHT_POSITION_CHANNEL );
		break;
#endif

// ------------------------------------------------------------------------------------
// Back / Fog Light
// ------------------------------------------------------------------------------------
#ifdef SIDE_RIGHT
	case CAN_MSG_LIGHT_BACK_ON:
		TLE8104E_setChannelOR( TLE8104E0, LIGHT_FOG_BACK_CHANNEL, 1 );
		break;

	case CAN_MSG_LIGHT_BACK_OFF:
		TLE8104E_setChannelOR( TLE8104E0, LIGHT_FOG_BACK_CHANNEL, 0 );
		break;
#endif

#ifdef SIDE_LEFT
	case CAN_MSG_LIGHT_FOG_ON:
		TLE8104E_setChannelOR( TLE8104E0, LIGHT_FOG_BACK_CHANNEL, 1 );
		break;

	case CAN_MSG_LIGHT_FOG_OFF:
		TLE8104E_setChannelOR( TLE8104E0, LIGHT_FOG_BACK_CHANNEL, 0 );
		break;
#endif

// ------------------------------------------------------------------------------------
// Break Light
// ------------------------------------------------------------------------------------
	case CAN_MSG_LIGHT_BREAK_ON:
		ITS5215L_on( ITS5215L0, LIGHT_BREAK_CHANNEL );
		break;
	case CAN_MSG_LIGHT_BREAK_OFF:
		ITS5215L_off( ITS5215L0, LIGHT_BREAK_CHANNEL );
		break;

// ------------------------------------------------------------------------------------
// Plate Light
// ------------------------------------------------------------------------------------
	case CAN_MSG_LIGHT_PLATE_ALL_ON:
	case CAN_MSG_LIGHT_PLATE_BACK_ON:
		TLE8104E_setChannelOR( TLE8104E0, LIGHT_PLATE_CHANNEL, 1 );
		break;

	case CAN_MSG_LIGHT_PLATE_ALL_OFF:
	case CAN_MSG_LIGHT_PLATE_BACK_OFF:
		TLE8104E_setChannelOR( TLE8104E0, LIGHT_PLATE_CHANNEL, 0 );
		break;

	default:
		return false;
	}

	return true;
}
#endif
