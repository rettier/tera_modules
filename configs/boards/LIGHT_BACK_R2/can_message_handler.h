#ifndef __LIGHT_FRONT_MESSAGE_HANDLER_H__
#define __LIGHT_FRONT_MESSAGE_HANDLER_H__

#include "stm32fx_modules.h"

extern bool handleMessage( uint8_t msg_id, uint8_t board_id, uint8_t* data, uint8_t data_length );

#endif // __LIGHT_FRONT_MESSAGE_HANDLER_H__
