#ifndef __MODULES_BOARD_BACK_DEF_H__
#define __MODULES_BOARD_BACK_DEF_H__

#include "stm32fx_modules.h"

#define LIGHT_SIGNAL_PIN PB12

#define LIGHT_POSITION_CHANNEL 1
#define LIGHT_PLATE_CHANNEL    2
#define LIGHT_BACK_FOG_CHANNEL 3
#define LIGHT_BREAK_CHANNEL    0


#define TLE8104E0_INIT { &PB11_, &PB14_, &PB15_, &PB13_, &PB10_, &PA3_, \
												 GPIO_PinSource3, GPIO_PortSourceGPIOA, EXTI_Line3, EXTI3_IRQn, \
												 SPI2, &TLE8104E0_info, \
												 RCC_APB1PeriphClockCmd, RCC_APB1Periph_SPI2 }

struct _TLE8104E_MyDef_;
EXT(struct _TLE8104E_MyDef_, TLE8104E0);

#endif // __MODULES_BOARD_DEF_H__
