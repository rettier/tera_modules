#include "modules_board_def.h"
#include "tle8104e.h"
#include "debug.h"

TLE8104EInfo_MyDef TLE8104E0_info = { 0, 0 };
INIT(TLE8104E_MyDef, TLE8104E0);

void EXTI3_IRQHandler(void)
{
  if(EXTI_GetITStatus(EXTI_Line3) != RESET)
  {
  	TLE8104E_irq_( TLE8104E0 );
    EXTI_ClearITPendingBit(EXTI_Line3);
  }
}
