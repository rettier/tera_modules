#include "can_message_handler.h"
#include "can_defines_light.h"

#include "its5215l.h"
#include "tle8104e.h"

extern volatile uint8_t blinker_on;

bool handleMessage( uint8_t msg_id, uint8_t board_id, uint8_t* data, uint8_t data_length )
{
	switch( msg_id )
	{
	// ------------------------------------------------------------------------------------
	// Signal Light
	// ------------------------------------------------------------------------------------
#ifdef SIDE_LEFT
case CAN_MSG_LIGHT_SIGNAL_LEFT_ON:
	blinker_on = 1;
	break;
case CAN_MSG_LIGHT_SIGNAL_LEFT_OFF:
	blinker_on = 0;
	break;
#endif

#ifdef SIDE_RIGHT
case CAN_MSG_LIGHT_SIGNAL_RIGHT_ON:
	blinker_on = 1;
	break;
case CAN_MSG_LIGHT_SIGNAL_RIGHT_OFF:
	blinker_on = 0;
	break;
#endif


	// ------------------------------------------------------------------------------------
	// Position Light
	// ------------------------------------------------------------------------------------
case CAN_MSG_LIGHT_POSITION_ALL_ON:
	TLE8104E_setChannelOR( TLE8104E0, LIGHT_POSITION_CHANNEL, 1 );
	break;
case CAN_MSG_LIGHT_POSITION_ALL_OFF:
	TLE8104E_setChannelOR( TLE8104E0, LIGHT_POSITION_CHANNEL, 0 );
	break;

#ifdef SIDE_LEFT
case CAN_MSG_LIGHT_POSITION_LEFT_ON:
	TLE8104E_setChannelOR( TLE8104E0, LIGHT_POSITION_CHANNEL, 1 );
	break;
case CAN_MSG_LIGHT_POSITION_LEFT_OFF:
	TLE8104E_setChannelOR( TLE8104E0, LIGHT_POSITION_CHANNEL, 0 );
	break;
#endif

#ifdef SIDE_RIGHT
case CAN_MSG_LIGHT_POSITION_RIGHT_ON:
	TLE8104E_setChannelOR( TLE8104E0, LIGHT_POSITION_CHANNEL, 1 );
	break;
case CAN_MSG_LIGHT_POSITION_RIGHT_OFF:
	TLE8104E_setChannelOR( TLE8104E0, LIGHT_POSITION_CHANNEL, 0 );
	break;
#endif


	// ------------------------------------------------------------------------------------
	// Plate light
	// ------------------------------------------------------------------------------------
case CAN_MSG_LIGHT_PLATE_ON:
	TLE8104E_setChannelOR( TLE8104E0, LIGHT_PLATE_CHANNEL, 1 );
	break;
case CAN_MSG_LIGHT_PLATE_OFF:
	TLE8104E_setChannelOR( TLE8104E0, LIGHT_PLATE_CHANNEL, 0 );
	break;


	// ------------------------------------------------------------------------------------
	// Fog / back light
	// ------------------------------------------------------------------------------------
#ifdef SIDE_RIGHT
case CAN_MSG_LIGHT_FOG_ON:
	TLE8104E_setChannelOR( TLE8104E0, LIGHT_BACK_FOG_CHANNEL, 1 );
	break;
case CAN_MSG_LIGHT_FOG_OFF:
	TLE8104E_setChannelOR( TLE8104E0, LIGHT_BACK_FOG_CHANNEL, 0 );
	break;
#else
case CAN_MSG_LIGHT_BACK_ON:
	TLE8104E_setChannelOR( TLE8104E0, LIGHT_BACK_FOG_CHANNEL, 1 );
	break;
case CAN_MSG_LIGHT_BACK_OFF:
	TLE8104E_setChannelOR( TLE8104E0, LIGHT_BACK_FOG_CHANNEL, 0 );
	break;
#endif


	// ------------------------------------------------------------------------------------
	// Fog / back light
	// ------------------------------------------------------------------------------------
	// can't work since we need high side switches

	default:
		return false;
	}

	return true;
}
