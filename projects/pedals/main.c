#include "gpio.h"
#include "timer.h"
#include "led.h"
#include "debug.h"
#include "delay.h"
#include "can.h"
#include "adc_dma2.h"

#include "board_cfg.h"

#define CAN_ERROR_ID 0x01
#define CAN_VALUE_ID 0x02

#define MIN_VALUE_2V5 282   //TODO recheck these values
#define MAX_VALUE_2V5 2539

#define MIN_VALUE_5V 399
#define MAX_VALUE_5V 3591

uint16_t trim5V( uint16_t v )
{
    if( v > MAX_VALUE_5V ) v = MAX_VALUE_5V;
    else if( v < MIN_VALUE_5V ) v = MIN_VALUE_5V;
    return v;
}

uint16_t trim2V5( uint16_t v )
{
    if( v > MAX_VALUE_2V5 ) v = MAX_VALUE_2V5;
    else if( v < MIN_VALUE_2V5 ) v = MIN_VALUE_2V5;
    return v;
}

uint16_t max( uint16_t a, uint16_t b )
{
	if( a > b )
		return a;
	return b;
}

uint16_t min( uint16_t a, uint16_t b )
{
	if( a > b )
		return b;
	return a;
}

void PIT_CAN_WRITEOUT_IRQ( )
{
	static uint16_t errors   = 0;
	static uint16_t good     = 0;
	static uint16_t disabled = 0;

	if (TIM_GetITStatus(PIT_CAN_WRITEOUT->timx_, TIM_IT_Update))
	{
		uint16_t fatal = 0;
		uint16_t correct = 1;

		TIM_ClearITPendingBit(PIT_CAN_WRITEOUT->timx_, TIM_IT_Update);

		uint16_t val5V = trim5V(GAS_ADC_val5V);
		uint16_t val2V5 = trim2V5(GAS_ADC_val2V5);
//		uint16_t val21 = trim(GAS_ADC_val21);
//		uint16_t val22 = trim(GAS_ADC_val22);

//		uint16_t diff1 = max(val5V, val2V5) - min(val5V, val2V5);
//		uint16_t diff2 = max(val21, val22) - min(val21, val22);
//		int16_t diff3 = max(GAS_ADC_val11, GAS_ADC_val12) - min(GAS_ADC_val22, GAS_ADC_val21);
//		if( diff3 < 0 )
//			diff3 = -diff3;

		uint32_t percent5V = 64*(val5V - MIN_VALUE_5V) / (MAX_VALUE_5V - MIN_VALUE_5V);
        uint32_t percent2V5 = 64*(val2V5 - MIN_VALUE_2V5) / (MAX_VALUE_2V5 - MIN_VALUE_2V5);
        int16_t percentDiff = percent5V - percent2V5;

        if(percentDiff < 0)
            percentDiff *= -1;
		// on error
//        if( diff1 > 10 || diff2 > 10 || diff3 > 30 )
        if( percentDiff > 64*5 )
		{
			correct = 0;
			LED_off( LED1 );

			// no good
			good    = 0;
			errors += 10;

			// fatal?
			if( errors > 100 )
			{
				LED_on( LED2 );
				disabled = 1;
				fatal    = 1;
			}
		}else if( disabled )
		{
			good++;
			if( good > 50 )
			{
				disabled = 0;
				errors   = 0;
				good     = 0;

				LED_off( LED2 );
			}
		}

		if( errors > 0 )
			errors--;

		if( !disabled && correct )
		{
//			uint32_t sum = val11 + val12 + val21 + val22 - ( 4 * MIN_VALUE );
		    // fixed point (5 numbers)
			uint32_t percentAvg = (percent2V5 + percent5V) / 2;
			if( percentAvg > 255 )
			    percentAvg = 255;

			uint8_t val[1] = { (uint8_t)percentAvg };
			CAN_writeData( CAN1, CAN_VALUE_ID, val, 1 );
		}

		if( fatal )
		{
			CAN_writeData( CAN1, CAN_ERROR_ID, 0, 0 );
		}
	}
}

int main(int argc, char** argv)
{
	LED_init();
	DELAY_init();
	LED_on( LED1 );

// inititalize CAN, no filters, just sending
	CAN_init(CAN_BOARD_PEDALS1, CAN1_RX_PB8, CAN1_TX_PB9, 0, 0, BAUD_500K);

	ADC_init(GAS_ADC);
	ADC_initChannel(GAS_ADC, GAS1_ADC_CHANNEL_1);
//	ADC_initChannel(GAS_ADC, GAS2_ADC_CHANNEL_1);
//	ADC_initChannel(GAS_ADC, GAS2_ADC_CHANNEL_2);
	ADC_initChannel(GAS_ADC, GAS1_ADC_CHANNEL_2);

	DELAY_us(5000); //wait for useful ADC values

	TIMER_initPeriodicInterrupt(PIT_CAN_WRITEOUT, 50, 1, 0);

	while (1)
	{
	    DELAY_us(1000000);
	    LED_toggle(LED1);
	}
}

// ----------------------------------------------------------------------------
