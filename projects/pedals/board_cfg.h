/*
 * board_cfg.h
 *
 *  Created on: Jun 6, 2014
 *      Author: dave
 */

#ifndef BOARD_CFG_H_
#define BOARD_CFG_H_

#include "adc_dma.h"

#define GAS_ADC             ADC_1
#define GAS1_ADC_CHANNEL_1	ADC_CH3
#define GAS1_ADC_CHANNEL_2	ADC_CH4
//#define GAS2_ADC_CHANNEL_1	ADC_CH3
//#define GAS2_ADC_CHANNEL_2	ADC_CH4
#define GAS_ADC_val5V       GAS_ADC->info_->avg_storage_[0]
#define GAS_ADC_val2V5      GAS_ADC->info_->avg_storage_[1]
//#define GAS_ADC_val21       GAS_ADC->info_->avg_storage_[2]
//#define GAS_ADC_val22       GAS_ADC->info_->avg_storage_[3]

#define PIT_CAN_WRITEOUT        TIM2_CH1_PA0
#define PIT_CAN_WRITEOUT_IRQ    TIM2_IRQHandler


#endif /* BOARD_CFG_H_ */
