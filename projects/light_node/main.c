#include "gpio.h"
#include "timer.h"
#include "led.h"
#include "debug.h"
#include "tle8104e.h"
#include "its5215l.h"
#include "delay.h"
#include "can.h"
#include "adc_dma2.h"
#include "can_message_handler.h"

// ----------------------------------------------------------------------------
void SysTick_Handler( );
#define SYSTICK_FREQUENCY_HZ       1000
static volatile uint32_t ticks;
void SysTick_Handler( )
{
	ticks++;
}
// ----------------------------------------------------------------------------
uint8_t data[8];
enum CAN_eMessageId msg_id;
enum CAN_eBoardId board_id;
uint8_t data_length;
volatile uint8_t blinker_on = 0;

void CAN1_RX_IRQHandler( )
{
	CAN_read( CAN1, &msg_id, &board_id, data, &data_length );
	handleMessage( msg_id, board_id, data, data_length );
	LED_toggle( LED2 );
}

int main( int argc, char** argv )
{
	SysTick_Config( SystemCoreClock / SYSTICK_FREQUENCY_HZ );

	LED_init( );
	DELAY_init( );
	TLE8104E_init( TLE8104E0 );
	ITS5215L_init( ITS5215L0 );

#ifdef SIDE_FRONT
#ifdef SIDE_RIGHT
	uint16_t board_id = CAN_BOARD_LIGHT_FRONT_RIGHT;
#else
	uint16_t board_id = CAN_BOARD_LIGHT_FRONT_LEFT;
#endif
#endif

#ifdef SIDE_BACK
#ifdef SIDE_RIGHT
	uint16_t board_id = CAN_BOARD_LIGHT_BACK_RIGHT;
#else
	uint16_t board_id = CAN_BOARD_LIGHT_BACK_LEFT;
#endif
#endif

	CAN_init( board_id, CAN1_RX_PB8, CAN1_TX_PB9, 0, 0, BAUD_500K );
	CAN_addMaskFilters16Bit( CAN1, CAN_BOARD_LIGHT_CONTROLLER, CAN_PARAM_BOARD_ID_MASK,
																 CAN_BOARD_LIGHT_CONTROLLER, CAN_PARAM_BOARD_ID_MASK );

	TIMER_initPWMOutput( TIM2_CH1_PA0, 10000 );
	TIMER_setPWMOutputDutyCycle( TIM2_CH1_PA0, 100.0f );

	uint32_t blinker_offset = 0;
	uint8_t blinking = 0;
	while(1)
	{
		uint32_t now = ticks;

		if( ticks % 1000 == 0 )
		{
			LED_on ( LED1 );
		}else if( ticks % 500 == 0 )
		{
			LED_off( LED1 );
		}

		if( blinker_on != blinking )
		{
			blinker_offset = ticks;
			blinking = blinker_on;
			TLE8104E_setChannelOR( TLE8104E0, LIGHT_SIGNAL_CHANNEL, blinking );
		}

		if( blinking )
		{
			uint32_t normalized = ticks - blinker_offset;
			if( normalized % 1000 == 0 )
			{
				TLE8104E_setChannelOR( TLE8104E0, LIGHT_SIGNAL_CHANNEL, 1 );
			}else if( normalized % 500 == 0 )
			{
				TLE8104E_setChannelOR( TLE8104E0, LIGHT_SIGNAL_CHANNEL, 0 );
			}
		}

		while( ticks == now );
	}
}


// ----------------------------------------------------------------------------
