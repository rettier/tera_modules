#include "gpio.h"
#include "timer.h"
#include "led.h"
#include "debug.h"
#include "tle8104e.h"
#include "its5215l.h"
#include "delay.h"
#include "can.h"
#include "adc_dma2.h"

// ----------------------------------------------------------------------------
static void Delay( __IO uint32_t nTime );
static void TimingDelay_Decrement( );
void SysTick_Handler( );

/* ----- SysTick definitions ----------------------------------------------- */
#define SYSTICK_FREQUENCY_HZ       1000
static __IO uint32_t uwTimingDelay;

void Delay(__IO uint32_t nTime)
{
	uwTimingDelay = nTime;
	while (uwTimingDelay != 0);
}

void TimingDelay_Decrement( )
{
	if( uwTimingDelay != 0x00 )
	{
		uwTimingDelay--;
	}
}

void SysTick_Handler( )
{
	TimingDelay_Decrement( );
}

void CAN1_RX_IRQHandler( )
{
	LOG_DEBUG("RX!");
}

uint32_t lastTime = 0;
uint16_t max = 0;
void adc_avg_hook( const ADC_MyDef *adc )
{
	if( adc->info_->avg_storage_[0] > max )
		max = adc->info_->avg_storage_[0];
}

int main( int argc, char** argv )
{
	SysTick_Config( SystemCoreClock / SYSTICK_FREQUENCY_HZ );

	LED_init( );
	DELAY_init( );
	TLE8104E_init( TLE8104E0 );
	ITS5215L_init( ITS5215L0 );

	//CAN_init( CAN_BOARD_TEST_1, CAN1_RX_PB8, CAN1_TX_PB9, 0, 0, BAUD_500K );

//	TLE8104E_setChannelsOR( TLE8104E0, 0x0 );
	TIMER_initPWMOutput( TIM2_CH1_PA0, 1000 );
	TIMER_setPWMOutputDutyCycle( TIM2_CH1_PA0, 100.0f );

	LOG_DEBUG("init done");

//	ITS5215L_on( ITS5215L0, 1 ); //
//	ITS5215L_on( ITS5215L0, 0 ); // ch 0: fernlicht, ch 1: abblend
//	ITS5215L_on( ITS5215L0, 0 );


	while(1)
	{
		Delay( 5000 );
		ITS5215L_toggle( ITS5215L0, 0 );

		LED_toggle( LED1 );
		LED_toggle( LED2 );

		ITS5215L_Error err = ITS5215L_getFault( ITS5215L0, 0 );
		if( err )
		{
			LOG_ERROR("ITS5215L Fault: %i, CH: %i", err, 0 );
			ITS5215L_clearFault( ITS5215L0, 0 );
		}

	}
}


// ----------------------------------------------------------------------------
