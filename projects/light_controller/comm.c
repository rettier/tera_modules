#include "comm.h"
#include "can.h"

void COMM_sendLightCommand( uint16_t message )
{
	CAN_writeData( CAN1, message, 0, 0 );
}
