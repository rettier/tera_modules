#include "gpio.h"
#include "timer.h"
#include "led.h"
#include "debug.h"
#include "delay.h"
#include "can.h"
#include "comm.h"
#include "can_defines_light.h"

// ----------------------------------------------------------------------------
void SysTick_Handler( );
#define SYSTICK_FREQUENCY_HZ       1000
static volatile uint32_t ticks;
void SysTick_Handler( )
{
	ticks++;
}
// ----------------------------------------------------------------------------
uint8_t data[8];
enum CAN_eMessageId msg_id;
enum CAN_eBoardId board_id;
uint8_t data_length;
volatile uint8_t blinker_on = 0;

void CAN1_RX_IRQHandler( )
{
	CAN_read( CAN1, &msg_id, &board_id, data, &data_length );
	LED_toggle( LED2 );
}

void USART_init( )
{
  RCC_APB2PeriphClockCmd( RCC_APB2Periph_USART1 | RCC_APB2Periph_AFIO, ENABLE );

  GPIO_PinRemapConfig( GPIO_Remap_USART1, ENABLE );

	GPIO_MyInitDef GPIO_InitStructure;
	GPIO_InitStructure.output_mode = GPIO_OUTPUT_PP;
	GPIO_InitStructure.pin_speed   = GPIO_SPEED_HIGH;
	GPIO_InitStructure.pin_mode    = GPIO_PIN_AF;
	GPIO_InitStructure.pull_mode   = GPIO_PULL_NONE;
	GPIO_init( PB6, &GPIO_InitStructure );

	GPIO_InitStructure.pin_mode = GPIO_PIN_AF_INPUT;
	GPIO_init( PB7, &GPIO_InitStructure );

	USART_InitTypeDef USART_InitStructure;
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_Even;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(USART1, &USART_InitStructure);
	USART_Cmd(USART1, ENABLE);
}

int main( int argc, char** argv )
{
	SysTick_Config( SystemCoreClock / SYSTICK_FREQUENCY_HZ );

	LED_init( );
	DELAY_init( );

	CAN_init( CAN_BOARD_LIGHT_CONTROLLER, CAN1_RX_PB8, CAN1_TX_PB9, 0, 0, BAUD_500K );
	CAN_addMaskFilters16Bit( CAN1, 0x0000, 0x0000, 0x0000, 0x0000 );

	USART_init( );

	DELAY_us( 5000000 );
	USART_ReceiveData( USART1 );

	while(1)
	{
		uint32_t now = ticks;
/*
		if( ticks % 1000 == 0 )
		{
			LED_on ( LED1 );
		}else if( ticks % 500 == 0 )
		{
			LED_off( LED1 );
		}
*/
		while( USART_GetFlagStatus( USART1, USART_FLAG_RXNE ) == RESET ) { }
		uint8_t id = USART_ReceiveData( USART1 ) & 0x3F;
		CAN_writeData( CAN1, id, 0, 0 );

		LED_toggle( LED1 );

		while( ticks == now );
	}
}


// ----------------------------------------------------------------------------
